var user = $('.user').DataTable();

function actSidebar(){
    // handling large device
    if($(window).width() > 510){
        if($("#sidebar").hasClass("col-md-3")){
            $("#sidebar").removeClass();
            $("#sidebar").addClass("col-md-1 act");
            $("span.title").hide();

            $("#content-page").removeClass();
            $("#content-page").addClass("col-md-11");
        } else{
            $("#sidebar").removeClass();
            $("#sidebar").addClass("col-md-3 act");
            $("span.title").show();

            $("#content-page").removeClass();
            $("#content-page").addClass("col-md-9");
        }
    }
    // handling small device
    if($('#sidebar').is(':hidden') && $(window).width() < 900){
        $("#sidebar").removeClass();
        $("#sidebar").addClass("col-md-3 act");
        $("#sidebar").show("slow");
        $("span.title").show();
        $("#content-page").removeClass();
        $("#content-page").addClass("col-md-9");
    } else if($(window).width() < 900){
        $("span.title").show();
        $("#sidebar").hide("slow");
        $("#content-page").removeClass();
        $("#content-page").addClass("col-md-9");
    }
}
function destroyData(table, id, base){
    swal({
        title: "Apakah anda yakin untuk menghapus data ini?",
        text: "Data will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirm",
        cancelButtonText: "Cancel",
    }).then((result) => {
         if (result.value) {
         $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : base +'config/process.php', // the url where we want to POST
            data: {table: table, id: id, method: 'delete'},
            dataType : 'json',
            success:function(data){
                if (data.json == 'berhasil') {
                    swal("Deleted!", "Data berhasil dihapus", "success");
                    setTimeout(function(){ location.reload(); }, 500);
                }
            }
         });

     }else{

     swal("Cancelled", "You Cancelled", "error");

     }    

 });
}
