<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] == 'admin'){
        header("Location: ../backend/index.php");
    } else{
//////////////////////////////////////////////////////////////////////
    include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');

?>
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php
include_once('include.php');
?>
<div class="col-md-9" id="content-page">
        <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        Data rawat jalan 
                    <div class="pull-right">
                    <small class="btn btn-primary btn-xs" onclick="addData()">
                    <i class="glyphicon glyphicon-plus" ></i> Ajukan rawat Jalan
                    </small>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                <table class="table table-striped table-bordered user">
                    <thead>
                        <tr>			
                            <th>No Rawat Jalan</th>
                            <th>No. RM</th>
                            <th>Nama Pasien</th>
                            <th>Status</th>
                            <th>Persetujuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $class = new Class_lib();
                    $sql = "SELECT * FROM rawat_jalan";
                    $query = mysqli_query($class->conn(), $sql);
                    $no = 1;
                    while ($row = mysqli_fetch_array($query)){ ?>
                        <tr>
                            <td><?php echo $row['no_rawat'];?></td>
                            <td><?php echo $row['id_regist'];?></td>
                            <td><?php echo $row['nm_lengkap'];?></td>
                            <td><?php echo $row['status'];?></td>
                            <td><?php echo $row['persetujuan'];?></td>
                            <td>
                            <center>
                             <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('rawat_jalan','<?php echo $row['no_rawat'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                            </center>
                            </td>
                        </tr>
                    <?php $no++; };?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="addRjalan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Pengajuan Rawat Jalan Pasien</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label for="" class="col-sm-4 control-label">Masukkan No. RM</label>
                <div class="col-sm-8">
                    <div class="input-group">
                    <input type="number" class="form-control" name="id_regist" id="id_regist">
                    <div class="input-group-addon" onclick="search('<?php echo $base_url;?>')"><i class="fa fa-search"></i></div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
            </div>
        </div>
        <hr>
        <div class="row" id="formAdd">
            <div class="col-md-6 col-sm-6 col-xs-6">
            <span class="form-horizontal">
                <form action="" method="post" id="rawat_jalan">
                <input type="hidden" name="add_data" value="rawat_jalan">
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">No.RM <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <input type="text" name="id_regist" id="id_regist" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">NIP / NRP</label>
                    <div class="col-sm-7">
                    <input type="text" name="nip_nrp"  id="nip_nrp" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">No. BPJS / Asuransi</label>
                    <div class="col-sm-7">
                    <input type="text" name="no_asuransi"  id="no_asuransi" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Nama pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <input type="text" name="nm_lengkap"  id="nm_lengkap" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Tgl. Lahir</label>
                    <div class="col-sm-7">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input type="text" class="form-control" name="tgl_lahir" id="datepicker" name="tgl_lahir">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Usia ( Tahun )</label>
                    <div class="col-sm-7">
                    <input type="number" name="usia"  id="usia" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                <label for="" class="col-sm-5 control-label">Diagnosa Sekunder 1</label>
                <div class="col-sm-7">
                <input type="text" name="diagnosa_1"  id="diagnosa_1" class="form-control">
                </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Diagnosa Sekunder 2</label>
                    <div class="col-sm-7">
                    <input type="text" name="diagnosa_2"  id="diagnosa_2" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Diagnosa Sekunder 3</label>
                    <div class="col-sm-7">
                    <input type="text" name="diagnosa_3"  id="diagnosa_3" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Diagnosa Sekunder 4</label>
                    <div class="col-sm-7">
                    <input type="text" name="diagnosa_4"  id="diagnosa_4" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Diagnosa Sekunder 5</label>
                    <div class="col-sm-7">
                    <input type="text" name="diagnosa_5"  id="diagnosa_5" class="form-control">
                    </div>
                </div>
            </span>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <span class="form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Status</label>
                    <div class="col-sm-7">
                    <input type="text" name="status"  id="status"  value="OPEN"  readonly class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">No. Registrasi</label>
                    <div class="col-sm-7">
                    <input type="text" name="no_rawat"  id="no_rawat" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Tgl. Registrasi</label>
                    <div class="col-sm-7">
                    <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input type="text" class="form-control" name="tgl_regist" id="datepicker1" value="<?php echo date('d-m-Y')?>" name="tgl_lahir">
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Lokasi <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <input type="text" name="lokasi"  id="lokasi" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Poliklinik <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <input type="text" name="poliklinik"  id="poliklinik" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Diagnosa Awal <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <input type="text" name="diagnosa_awal"  id="diagnosa_awal" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Dokter</label>
                    <div class="col-sm-7">
                    <input type="text" name="dokter"  id="dokter" class="form-control">
                    </div>
                </div> <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Catatan</label>
                    <div class="col-sm-7">
                    <textarea name="catatan"  id="catatan" cols="6" rows="3" class="form-control "></textarea>
                    </div>
                </div>
                <h4>Jenis Pembayaran</h4>
                <hr>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Group pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <select name="id_group" id="id_group" class="form-control" required="">
                        <?php
                        $class = new Class_lib();
                        $sql = "SELECT * FROM group_pasien order by nm_group desc";
                        $query = mysqli_query($class->conn(), $sql);
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)){ ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_group'] ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-5 control-label">Golongan pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                    <div class="col-sm-7">
                    <select name="id_golongan" id="id_golongan" class="form-control" required="">
                        <?php
                        $class = new Class_lib();
                        $sql = "SELECT * FROM golongan order by nm_golongan desc";
                        $query = mysqli_query($class->conn(), $sql);
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)){ ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_golongan'] ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                <figure class="highlight"><pre><code class="language-bash pull-right" data-lang="bash"><button class="btn btn-primary" type="submit">Ajukan</button></code></pre></figure>
                </div>
                </form>
                </span>
            </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Lihat data Pasien</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
     <div class="form-group">
        <label for="">No. RM</label>
        <p id="id_regist" name="id_regist"></p>
     </div>
     <div class="form-group">
        <label for="">NIP / NRP</label>
        <p id="nip_nrp" name="nip_nrp"></p>
     </div>
     <div class="form-group">
        <label for="">No. BPJS / Asuransi</label>
        <p id="no_asuransi" name="no_asuransi"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Pasien</label>
        <p id="nm_lengkap" name="nm_lengkap"></p>
     </div>
     <div class="form-group">
        <label for="">Tgl. Lahir</label>
        <p id="tgl_lahir" name="tgl_lahir"></p>
     </div>
     <div class="form-group">
        <label for="">Usia</label>
        <p id="usia" name="usia"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Sekunder 1</label>
        <p id="diagnosa_1" name="diagnosa_1"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Sekunder 2</label>
        <p id="diagnosa_2" name="diagnosa_2"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Sekunder 3</label>
        <p id="diagnosa_3" name="diagnosa_3"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Sekunder 4</label>
        <p id="diagnosa_4" name="diagnosa_4"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Sekunder 5</label>
        <p id="diagnosa_5" name="diagnosa_5"></p>
     </div>

    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="form-group">
        <label for="">Status</label>
        <p id="status" name="status"></p>
     </div>
     <div class="form-group">
        <label for="">No. Registrasi</label>
        <p id="no_rawat" name="no_rawat"></p>
     </div>
     <div class="form-group">
        <label for="">Lokasi</label>
        <p id="lokasi" name="lokasi"></p>
     </div>
     <div class="form-group">
        <label for="">Poliklinik</label>
        <p id="poliklinik" name="poliklinik"></p>
     </div>
     <div class="form-group">
        <label for="">Diagnosa Awal</label>
        <p id="diagnosa_awal" name="diagnosa_awal"></p>
     </div>
     <div class="form-group">
        <label for="">Dokter</label>
        <p id="dokter" name="dokter"></p>
     </div>
     <div class="form-group">
        <label for="">Catatan</label>
        <p id="catatan" name="catatan"></p>
     </div>
     <h4>Jenis Pembayaran</h4>
     <hr>
     <div class="form-group">
        <label for="">Group Pasien</label>
        <p id="id_group" name="id_group"></p>
     </div>
     <div class="form-group">
        <label for="">Golongan Pasien</label>
        <p id="id_golongan" name="id_golongan"></p>
     </div>
     <div class="form-group">
        <label for="">Persetujuan Pengajuan</label>
        <br>
        <span id="persetujuan" name="persetujuan"></span>
     </div>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<!-- {{-- modal edit data end--}} -->
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php  
?>
<?php
    include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
    }
} else { 
    header("Location: ../index.php"); // Memaksa ke halaman login jikalau bukan admin 
} 
?>
<script>
function addData(){
    $('#addRjalan').modal('show');
}

$(document).ready(function(){
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'rawat_jalan', no_rawat : 'rawat', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#no_rawat').val(data.json);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
});
// stop the form from submitting the normal way and refreshing the page
$("#formAdd").hide();
//  set datepicker
$("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
    beforeShow: function() {
        setTimeout(function(){
            $('.ui-datepicker').css('z-index', 99999999999999);
        }, 0);
    }
});
//  set datepicker
$("#datepicker1").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
    beforeShow: function() {
        setTimeout(function(){
            $('.ui-datepicker').css('z-index', 99999999999999);
        }, 0);
    }
});
});
function search(base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: 'registrasi', id: $('#addRjalan input#id_regist').val(), method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $("#formAdd").show("slow");
            // masukkan ke text field

            var tl = (data.json.tgl_lahir).split("-");
                tl = tl[2]+'-'+tl[1]+'-'+tl[0];
            console.log(tl);
            $('#rawat_jalan input#id_regist').val(data.json.id_regist);
            $('#rawat_jalan input#nm_lengkap').val(data.json.nm_lengkap);
            $('#rawat_jalan input#datepicker').val(tl);
            $('#rawat_jalan input#nm_lengkap').val(data.json.nm_lengkap);
        } else{
            // $('#rawat_jalan')[0].reset();
            $("#formAdd").hide("slow");
            Swal('Oops...', data.msg, 'error');
        }
    }
    });
}
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, no_rawat: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
             // generate data
            $("#editModal p#id_golongan").html(data.json.golongan);
            $("#editModal p#id_group").html(data.json.groups);
            // from db
            $("#editModal p#id_regist").html(data.json.id_regist);
            $("#editModal p#nip_nrp").html(data.json.nip_nrp);
            $("#editModal p#no_asuransi").html(data.json.no_asuransi);
            $("#editModal p#nm_lengkap").html(data.json.nm_lengkap);
            $("#editModal p#tgl_lahir").html(data.json.tgl_lahir);
            $("#editModal p#usia").html(data.json.usia);
            $("#editModal p#diagnosa_1").html(data.json.diagnosa_1);
            $("#editModal p#diagnosa_2").html(data.json.diagnosa_2);
            $("#editModal p#diagnosa_3").html(data.json.diagnosa_3);
            $("#editModal p#diagnosa_4").html(data.json.diagnosa_4);
            $("#editModal p#diagnosa_5").html(data.json.diagnosa_5);
            $("#editModal p#status").html(data.json.status);
            $("#editModal p#no_rawat").html(data.json.no_rawat);
            $("#editModal p#tgl_regist").html(data.json.tgl_regist);
            $("#editModal p#lokasi").html(data.json.lokasi);
            $("#editModal p#poliklinik").html(data.json.poliklinik);
            $("#editModal p#diagnosa_awal").html(data.json.diagnosa_awal);
            $("#editModal p#dokter").html(data.json.dokter);
            $("#editModal p#catatan").html(data.json.catatan);
            $("#editModal p#persetujuan").html(data.json.persetujuan);
            if(data.json.persetujuan == 'diterima' && data.json.status == "OPEN"){
                $("#editModal span#persetujuan").html('<form method="post" id="edit_data"> <input type="hidden" name="edit_data" value="rawat_jalan"><input type="hidden" name="primary" value="no_rawat"> <input type="hidden" id="id_data" name="id_data"><select name="status" id="status" class="form-control"><option>CLOSE</option></select><br><button class="btn btn-primary" type="type="button" onclick="updateRJ()">Perbaharui</button></form>');
                $("#editModal input#id_data").val(data.json.no_rawat);
            } else{
                $("#editModal span#persetujuan").html(data.json.persetujuan);
            }
        }
    }
    });
}
// process the form
$('form#rawat_jalan').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#rawat_jalan').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    Swal(
                    'Good job!',
                    data.msg,
                    'success'
                    )
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    Swal('Oops...', data.msg, 'error');
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
// process edit form
function updateRJ(){
    var formData = new FormData($("#editModal form#edit_data")[0]);
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        : $('#editModal form#edit_data').serialize(), // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                Swal(
                'Good job!',
                data.msg,
                'success'
                )
                setTimeout(function(){ location.reload(); }, 1000);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
    });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
    
}
</script>