<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] == 'admin'){
        header("Location: ../backend/index.php");
    } else{
//////////////////////////////////////////////////////////////////////
    include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');

?>
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php
include_once('include.php');
?>
<div class="col-md-9" id="content-page">
        <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        Form Registrasi Pasien 
                    <div class="pull-right">
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Registrasi Pasien</a></li>
                    <li><a data-toggle="tab" href="#menu1">Data Pasien</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                    <br>
                    <form action="" method="post" id="registrasi">
                    <input type="hidden" name="add_data" value="registrasi">
                    <div class="row">
<!-- ////////////////////////////////////////////////////////////////////// -->
                        <div class="col-md-6">
                        <span class="form-horizontal">
                        <div class="form-group">
                            <label for="id_regist" class="col-sm-4 control-label">No. RM</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="id_regist" id="id_regist" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">RS  <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="nm_rs" id="nm_rs" class="form-control" required="">
                                <option value="RSPAD" selected>RSPAD</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Awalan Nama <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                             <select name="id_awalan" id="id_awalan" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM nm_awalan order by id desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['awalan'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Nama <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="nm_lengkap" name="nm_lengkap" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Tempat Lahir <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <input type="text" id="tmpt_lahir" name="tmpt_lahir" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Tanggal Lahir</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="datepicker" value="<?php echo date('d-m-Y')?>" name="tgl_lahir">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Jenis Kelamin</label>
                            <div class="col-sm-8">
                            <select name="jns_kelamin" id="jns_kelamin"  class="form-control">
                                <option value="laki - laki">Laki - Laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Alamat</label>
                            <div class="col-sm-8">
                            <textarea name="alamat" id="alamat" cols="6" rows="3" class="form-control "></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Warga Negara</label>
                            <div class="col-sm-8">
                             <select name="id_negara" id="id_negara" class="form-control" required="">
                             <option value="">Pilih</option>
                                <?php
                                $conn = mysqli_connect($host = 'localhost', $username = 'root', $password ='', $database = 'db_rspad');
                                $sql = "SELECT * FROM negara";
                                $query = mysqli_query($conn, $sql);
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_negara'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Provinsi <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_province" id="id_province" class="form-control"  required disabled>
                            <option value="">Pilih</option>
                            <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM provinces order by name desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
                            <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Kota <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_city" id="id_city" required disabled class="form-control">
                            <option value="">Pilih</option>
                            </select>
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Kabupaten</label>
                            <div class="col-sm-8">
                            <select name="id_district" id="id_district" required disabled class="form-control">
                            <option value="">Pilih</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">desa</label>
                            <div class="col-sm-8">
                            <select name="id_village" id="id_village" required disabled class="form-control">
                            <option value="">Pilih</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Suku</label>
                            <div class="col-sm-8">
                            <input type="text" name="suku" id="suku" class="form-control">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Status Perkawinan <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="status" id="status" class="form-control">
                                <option value="lajang">Lajang</option>
                                <option value="menikah">Menikah</option>
                                <option value="janda/duda">Janda / Duda</option>
                            </select>
                            </div>
                        </div>

                        </span>
                        </div>
<!-- ////////////////////////////////////////////////////////////////////// -->
                        <div class="col-md-6">
                        <span class="form-horizontal">
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Agama <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam">Islam</option>
                                <option value="kristen katolik">Kristen Katolik</option>
                                <option value="kristen protestan">Kristen Protestan</option>
                                <option value="hindu">Hindu</option>
                                <option value="budha">Budha</option>
                                <option value="konghucu">Konghucu</option>
                                <option value="kristen">Kristen</option>
                                <option value="lain- lain">Lain - lain</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Telepon Pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <input type="text" name="tlp_pasien" id="tlp_pasien" class="form-control" required="" minlength="6" maxlength="15">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Telepon Keluarga <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="tlp_keluarga" id="tlp_keluarga" required="" minlength="6" maxlength="15">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Pekerjaan <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_pekerjaan" id="id_pekerjaan" class="form-control" required>
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM pekerjaan order by nm_pekerjaan desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_pekerjaan'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Pendidikan</label>
                            <div class="col-sm-8">
                            <select name="id_pendidikan" id="id_pendidikan" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM pendidikan order by nm_pendidikan desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_pendidikan'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Golongan Darah</label>
                            <div class="col-sm-8">
                            <select name="gol_darah" id="gol_darah" class="form-control" required="">
                              <option value="a">A</option>
                              <option value="b">B</option>
                              <option value="ab">AB</option>
                              <option value="o">O</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">NRP / NIP</label>
                            <div class="col-sm-8">
                            <input type="text" name="no_pengenal" id="no_pengenal" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Angkatan <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_angkatan" id="id_angkatan" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM angkatan order by nm_angkatan desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_angkatan'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Pangkat / Golongan</label>
                            <div class="col-sm-8">
                            <input type="text" name="pgkt_gol" id="pgkt_gol" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Kesatuan</label>
                            <div class="col-sm-8">
                            <input type="text" name="kesatuan" id="kesatuan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Jabatan</label>
                            <div class="col-sm-8">
                            <input type="text" name="jabatan" id="jabatan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Group pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_group" id="id_group" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM group_pasien order by nm_group desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_group'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Golongan pasien <i class="fa fa-exclamation-circle text-danger"></i></label>
                            <div class="col-sm-8">
                            <select name="id_golongan" id="id_golongan" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM golongan order by nm_golongan desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_golongan'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">No. BPJS / Asuransi</label>
                            <div class="col-sm-8">
                            <input type="text" name="no_asuransi" id="no_asuransi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Status pasien </label>
                            <div class="col-sm-8">
                            <select name="id_stat_pasien" id="id_stat_pasien" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM stat_pasien order by nm_status desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_status'] ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        </span>
                        </div>
<!-- ////////////////////////////////////////////////////////////////////// -->
<!-- second start -->
                        <div class="col-md-6">
                        <h4>Informasi Keluarga Dekat</h4>
                        <hr>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#keluarga1">Keluarga 1</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="keluarga1" class="tab-pane fade in active">
                            <br>
                            <span class="form-horizontal">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nama <i class="fa fa-exclamation-circle text-danger"></i></label>
                                <div class="col-sm-8">
                                <input type="text" name="nm_wakil" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Alamat <i class="fa fa-exclamation-circle text-danger"></i></label>
                                <div class="col-sm-8">
                                <textarea name="almt_wakil" id="almt_wakil" cols="6" rows="3" class="form-control" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">No. Telepon <i class="fa fa-exclamation-circle text-danger"></i></label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" name="tlp_wakil" id="tlp_wakil" required="" minlength="6" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">No. HP <i class="fa fa-exclamation-circle text-danger"></i></label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" name="hp_wakil" id="hp_wakil" required="" minlength="6" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Hubungan keluarga <i class="fa fa-exclamation-circle text-danger"></i></label>
                                <div class="col-sm-8">
                                <select name="id_hub_keluarga" id="id_hub_keluarga" class="form-control" required="">
                                    <?php
                                    $class = new Class_lib();
                                    $sql = "SELECT * FROM stat_pasien order by nm_status desc";
                                    $query = mysqli_query($class->conn(), $sql);
                                    $no = 1;
                                    while ($row = mysqli_fetch_array($query)){ ?>
                                    <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_status'] ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                            </span>
                            </div>
                            <div id="keluarga2" class="tab-pane fade">
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <h4>Perhatian Khusus</h4>
                        <hr>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pasien Memiliki Alergi</label>
                            <i class="fa fa-exclamation-circle text-danger"></i>
                            <br>
                            <input type="radio" name="psn_alergi" value="tidak" required=""> Tidak <input type="radio" name="psn_alergi" value="ya" required=""> Ya
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Riwayat Penyakit pasien</label>
                            <i class="fa fa-exclamation-circle text-danger"></i>
                            <select name="id_riwayat" id="id_riwayat" class="form-control" required="">
                                <?php
                                $class = new Class_lib();
                                $sql = "SELECT * FROM riwayat_penyakit order by nm_penyakit desc";
                                $query = mysqli_query($class->conn(), $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)){ ?>
                                <option value="<?php echo $row['id'] ?>"><?php echo $row['nm_penyakit'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                        <figure class="highlight"><pre><code class="language-bash pull-right" data-lang="bash"><button class="btn btn-primary" type="submit">Simpan</button></code></pre></figure>
                        </div>
                        </span>
                        </form>
                        </div>
<!-- second end -->
                    </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                    <br>
<!-- ////////////////////////////////////////////////////////////////////// -->
                    <table class="table table-striped table-bordered user table-condensed">
                        <thead>
                            <tr>			
                                <th>No. RM</th>
                                <th>Nama</th>
                                <th>Tempat, Tgl Lahir</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $class = new Class_lib();
                        $sql = "SELECT * FROM registrasi";
                        $query = mysqli_query($class->conn(), $sql);
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)){ ?>
                            <tr>
                                <td><?php echo $row['id_regist'];?></td>
                                <td><?php echo $row['nm_lengkap'];?></td>
                                <td><?php echo $row['tmpt_lahir'].', '.$row['tgl_lahir'];?></td>
                                <td><?php echo $row['alamat'];?></td>

                                <td>
                                <center>
                                <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('registrasi','<?php echo $row['id_regist'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                                </center>
                                </td>
                            </tr>
                        <?php $no++; };?>
                        </tbody>
                    </table>
<!-- ////////////////////////////////////////////////////////////////////// -->
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Lihat data Pasien</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
     <div class="form-group">
        <label for="">No. RM</label>
        <p id="id_regist" name="id_regist"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Rumah Sakit</label>
        <p id="nm_rs" name="nm_rs"></p>
     </div>
     <div class="form-group">
        <label for="">Awalan Nama</label>
        <p id="id_awalan" name="id_awalan"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Lengkap</label>
        <p id="nm_lengkap" name="nm_lengkap"></p>
     </div>
     <div class="form-group">
        <label for="">Tempat Lahir</label>
        <p id="tmpt_lahir" name="tmpt_lahir"></p>
     </div>
     <div class="form-group">
        <label for="">Tanggal lahir</label>
        <p id="tgl_lahir" name="tgl_lahir"></p>
     </div>
     <div class="form-group">
        <label for="">Jenis Kelamin</label>
        <p id="jns_kelamin" name="jns_kelamin"></p>
     </div>
     <div class="form-group">
        <label for="">Alamat</label>
        <p id="alamat" name="alamat"></p>
     </div>
     <div class="form-group">
        <label for="">Kewarganegaraan</label>
        <p id="id_negara" name="id_negara"></p>
     </div>
     <div class="form-group">
        <label for="">Provinsi</label>
        <p id="id_province" name="id_province"></p>
     </div>
     <div class="form-group">
        <label for="">Kota</label>
        <p id="id_city" name="id_city"></p>
     </div>
     <div class="form-group">
        <label for="">Kabupaten</label>
        <p id="id_district" name="id_district"></p>
     </div>
     <div class="form-group">
        <label for="">Desa</label>
        <p id="id_village" name="id_village"></p>
     </div>
     <div class="form-group">
        <label for="">Suku</label>
        <p id="suku" name="suku"></p>
     </div>
     <div class="form-group">
        <label for="">Status Perkawinan</label>
        <p id="status" name="status"></p>
     </div>
     <div class="form-group">
        <label for="">Agama</label>
        <p id="agama" name="agama"></p>
     </div>

    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="form-group">
        <label for="">Telp Pasien</label>
        <p id="tlp_pasien" name="tlp_pasien"></p>
     </div>
     <div class="form-group">
        <label for="">Telp Keluarga</label>
        <p id="tlp_keluarga" name="tlp_keluarga"></p>
     </div>
     <div class="form-group">
        <label for="">Pekerjaan</label>
        <p id="id_pekerjaan" name="id_pekerjaan"></p>
     </div>
     <div class="form-group">
        <label for="">Pendidikan</label>
        <p id="id_pendidikan" name="id_pendidikan"></p>
     </div>
     <div class="form-group">
        <label for="">Golongan Darah</label>
        <p id="gol_darah" name="gol_darah"></p>
     </div>
     <div class="form-group">
        <label for="">Angkatan</label>
        <p id="id_angkatan" name="id_angkatan"></p>
     </div>
     <div class="form-group">
        <label for="">Group</label>
        <p id="id_group" name="id_group"></p>
     </div>
     <div class="form-group">
        <label for="">Golongan</label>
        <p id="id_golongan" name="id_golongan"></p>
     </div>
     <div class="form-group">
        <label for="">Status Pasien</label>
        <p id="id_stat_pasien" name="id_stat_pasien"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Wakil</label>
        <p id="nm_wakil" name="nm_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Alamat Wakil</label>
        <p id="almt_wakil" name="almt_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Telp Wakil</label>
        <p id="tlp_wakil" name="tlp_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">HP Wakil</label>
        <p id="hp_wakil" name="hp_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Hubungan Keluarga</label>
        <p id="id_hub_keluarga" name="id_hub_keluarga"></p>
     </div>
     <div class="form-group">
        <label for="">Pasien Alergi</label>
        <p id="psn_alergi" name="psn_alergi"></p>
     </div>
     <div class="form-group">
        <label for="">Riwayat Penyakit</label>
        <p id="id_riwayat" name="id_riwayat"></p>
     </div>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<!-- {{-- modal edit data end--}} -->
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php  
?>
<?php
    include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
    }
} else { 
    header("Location: ../index.php"); // Memaksa ke halaman login jikalau bukan admin 
} 
?>
<script>
$(document).ready(function(){
//  set datepicker
$("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'registrasi', id_regist : 'registrasi', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#id_regist').val(data.json);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
});
// stop the form from submitting the normal way and refreshing the page

// saat memilih indonesia provinsi active
$("#id_negara").change(function(){
var id_negara = $(this).val();
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'negara', id_negara : id_negara, method: 'negara'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
               $("#id_province").removeAttr("disabled");
               $('#id_province').get(0).selectedIndex = 0;

            }else{
                $('#id_province').get(0).selectedIndex = 0;
                $("select#id_province").attr('disabled','disabled');
                $('#id_city').get(0).selectedIndex = 0;
                $("select#id_city").attr('disabled','disabled');
                $('#id_district').get(0).selectedIndex = 0;
                $("select#id_district").attr('disabled','disabled');
                $('#id_village').get(0).selectedIndex = 0;
                $("select#id_village").attr('disabled','disabled');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
    });
  });
// generate cities
$("#id_province").change(function(){
    var id_province = $(this).val();
    if(id_province == ''){
        $("#id_city").attr('disabled','disabled');
        $('#id_city').get(0).selectedIndex = 0;
    } else{
        $("#id_city").removeAttr("disabled");
        $('#id_city').get(0).selectedIndex = 0;
        $("#id_district").attr('disabled','disabled');
        $('#id_district').get(0).selectedIndex = 0;
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'cities', id_province : id_province, method: 'cities'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_city').find('option').remove().end();
                    $('#id_city').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_city')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
});
// generate district
$("#id_city").change(function(){
    var id_city = $(this).val();
    if(id_city == ''){
        $("#id_district").attr('disabled','disabled');
        $('#id_district').get(0).selectedIndex = 0;
    } else{
        $("#id_district").removeAttr("disabled");
        $('#id_district').get(0).selectedIndex = 0;
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'districts', id_city : id_city, method: 'district'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_district').find('option').remove().end();
                    $('#id_district').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_district')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
});
// generate village
$("#id_district").change(function(){
    var id_district = $(this).val();
    if(id_district == ''){
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
    } else{
        $("#id_village").removeAttr("disabled");
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'villages', id_district : id_district, method: 'village'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_village').find('option').remove().end();
                    $('#id_village').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_village')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
 });
});

$("form").submit(function(e){
// process the form
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        : $('form#registrasi').serialize(), // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                Swal(
                'Good job!',
                data.msg,
                'success'
                )
                setTimeout(function(){ location.reload(); }, 1000);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
    });
// stop the form from submitting the normal way and refreshing the page
e.preventDefault();
});
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id_regist: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            // generate data
            $("#editModal p#id_regist").html(data.json.id_regist);
            $("#editModal p#no_rs").html(data.json.no_rs);
            $("#editModal p#id_awalan").html(data.json.awalan);
            $("#editModal p#id_negara").html(data.json.negara);
            $("#editModal p#id_province").html(data.json.provinsi);
            $("#editModal p#id_city").html(data.json.kota);
            $("#editModal p#id_district").html(data.json.kabupaten);
            $("#editModal p#id_village").html(data.json.desa);
            $("#editModal p#id_pekerjaan").html(data.json.pekerjaan);
            $("#editModal p#id_pendidikan").html(data.json.pendidikan);
            $("#editModal p#id_angkatan").html(data.json.angkatan);
            $("#editModal p#id_golongan").html(data.json.golongan);
            $("#editModal p#id_group").html(data.json.groups);
            $("#editModal p#id_stat_pasien").html(data.json.stat);
            $("#editModal p#id_riwayat").html(data.json.riwayat);
            $("#editModal p#id_hub_keluarga").html(data.json.hub);
            // dari db
            $("#editModal p#nm_rs").html(data.json.nm_rs);
            $("#editModal p#nm_lengkap").html(data.json.nm_lengkap);
            $("#editModal p#tmpt_lahir").html(data.json.tmpt_lahir);
            $("#editModal p#tgl_lahir").html(data.json.tgl_lahir);
            $("#editModal p#jns_kelamin").html(data.json.jns_kelamin);
            $("#editModal p#alamat").html(data.json.alamat);
            $("#editModal p#suku").html(data.json.suku);
            $("#editModal p#status").html(data.json.status);
            $("#editModal p#agama").html(data.json.agama);
            $("#editModal p#tlp_pasien").html(data.json.tlp_pasien);
            $("#editModal p#tlp_keluarga").html(data.json.tlp_keluarga);
            $("#editModal p#gol_darah").html(data.json.gol_darah);
            $("#editModal p#nm_wakil").html(data.json.nm_wakil);
            $("#editModal p#almt_wakil").html(data.json.almt_wakil);
            $("#editModal p#tlp_wakil").html(data.json.tlp_wakil);
            $("#editModal p#hp_wakil").html(data.json.hp_wakil);
            $("#editModal p#psn_alergi").html(data.json.psn_alergi);

        }
    }
    });
}
</script>