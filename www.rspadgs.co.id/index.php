<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] == 'admin'){
        header("Location: backend/index.php");
    } else{
        header("Location: frontend/index.php");
    }
} else{    
include_once('layout/header.php');
?>
<!-- ===================================================== -->
<style type="text/css">
    body {
        background-image: url('assets/images/bg-01.jpg');
        background-size: cover;
    }
    .login {
        margin: 1px auto;
        width: 320px;
        padding: 10px 20px 20px 20px;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 1px 2px;
    }
    input[type=text], input[type=password] {
        margin: 5px auto;
        width: 100%;
        padding: 10px;
    }
    input[type=submit] {
        margin: 7px auto;
        float: right;
        padding: 5px;
        width: 90px;
        border: 1px solid #fff;
        color: #fff;
        background: transparent;
        cursor: pointer;
    }
    .login100-form-title {
        display: block;
        font-size: 14px;
        color: #333333;
        line-height: 1.0;
        text-align: left;
        color: #fff;
    }
    .p-b-49 {
        padding-bottom: 8px;
    }
    #top{
        margin-top: 60px;
        margin-bottom: 20px;
        color: #fff;
        text-align: center;
    }
    img{
        border-radius: 20px;
    }
    h4{
        font-size: 30px;
    }
    </style>
<!-- ===================================================== -->
<body>
    <div id="top">
        <h4>Pelayanan instalasi gawat darurat pada rumah <br> sakit rspad gatot subroto</h4>
        <img src="assets/images/rspad.jpg" alt="">
    </div>
<div class="login">
<span class="login100-form-title p-b-49">SIGN IN TO YOUR ACCOUNT :</span>
<form method="post">
    <input type="text" name="id_user" placeholder="Masukan id user anda" minlength="6" class="form-control" required>
    <input type="password" name="password" placeholder="Masukan password" minlength="6" class="form-control" required>
    <input type="hidden" name="login" value="login">
    <input type="submit" class="btn" name="login" value="Kirim"> <br>
    
</form>
</div>

<!-- Menyisipkan bagian bawah website dari external file -->
<?php
include_once('layout/footer.php');
?>
<script>
$(document).ready(function() {
// process the form
$('form').submit(function(event) {
    // process the form
    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'config/process.php', // the url where we want to POST
            data        : $('form').serialize(), // our data object
            success: function (data) {
                    data = JSON.parse(data);
                    if(data.error == 0) {
                        if(data.json.hak_akses == 'admin'){
                            setTimeout(function(){ window.location.href= 'backend/index.php'; }, 1000);
                        } else{
                            setTimeout(function(){ window.location.href= 'frontend/index.php'; }, 1000);
                        }
                    }else{
                        Swal('Oops...', data.msg, 'error');
                    }

                },error:function(){
                    new PNotify({
                        title: 'Error',
                        text: 'Error. Please check your connection',
                        type: 'error'
                    });
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }
            });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
  });
});
</script>
<?php } ?>