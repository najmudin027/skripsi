<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] != 'admin'){
        header("Location: ../frontend/index.php");
    } else{
//////////////////////////////////////////////////////////////////////
    include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');

?>
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php
include_once('include.php');
?>
<div class="col-md-9" id="content-page">
        <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        Data Pengguna 
                    <div class="pull-right">
                    <small class="btn btn-primary btn-xs" onclick="addData()">
                    <i class="glyphicon glyphicon-plus" ></i> Tambah Pengguna
                    </small>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                <table class="table table-striped table-bordered user">
                    <thead>
                        <tr>			
                            <th>#</th>
                            <th>ID User</th>
                            <th>Nama</th>
                            <th>Hak akses</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $class = new Class_lib();
                    $sql = "SELECT * FROM user";
                    $query = mysqli_query($class->conn(), $sql);
                    $no = 1;
                    while ($row = mysqli_fetch_array($query)){ ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['id_user'];?></td>
                            <td><?php echo $row['nm_user'];?></td>
                            <td><?php echo $row['hak_akses'];?></td>
                            <td>
                            <center>
                             <a class="btn btn-danger btn-sm btnEdit" href="javascript:void(0)"  onclick="deleteData('user','<?php echo $row['id_user'];?>', '<?php echo $base_url ?>')"><i class="fa fa-trash"></i></a>
                            </center>
                            </td>
                        </tr>
                    <?php $no++; };?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- {{-- modal data edit start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Tambah data pengguna<span id="nm_user"></span> </h4>
      </div>
      <div class="modal-body">
      <form method="post">
        <?php
            $sql = "SELECT max(`id_user`) as id_user FROM user where left(id_user, 3) = 'PTG'";
            $query = mysqli_query($class->conn(), $sql);
            $getId = mysqli_fetch_array($query);
            if($getId['id_user'] == null or $getId['id_user'] == ''){
                $id_user = "PTG0001";
           }else{
            $id_user            = (int) substr($getId['id_user'], 3, 6)+1;
            $char                = "PTG";
            $id_user            = $char . sprintf("%04s", $id_user);
           }
        ?>
        <input type="hidden" name="add_data" value="user">
        <input type="hidden" name="id_user"  id="id_user" value="<?php echo $id_user ?>">
        <div class="form-group">
            <label for="">Hak akses pengguna</label>
            <select name="hak_akses" id="hak_akses" class="form-control">
                <option value="petugas">Petugas</option>
                <option value="admin">Admin</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Nama pengguna</label>
            <input type="text" name="nm_user" placeholder="Masukan nama pengguna" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Password pengguna</label>
            <input type="password" name="password" placeholder="Masukan password" minlength="6" class="form-control" required>
        </div>
        <input type="submit" class="btn btn-primary" value="Kirim"> <br>
            
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal data edit end--}} -->

<!-- ////////////////////////////////////////////////////////////////////// -->
<?php  
?>
<?php
    include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
    }
} else { 
    header("Location: ../index.php"); // Memaksa ke halaman login jikalau bukan admin 
} 
?>
<script>
    // generate id user
$(document).ready(function(){
$('#hak_akses').change(function(){
    var hak_akses = $(this).val();
    // process the form
    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
            data        :{table: 'user', hak_akses: hak_akses, method: 'generate'}, // our data object
            success: function (data) {
                    data = JSON.parse(data);
                    if(data.error == 0) {
                        $('#id_user').val(data.json);
                    }else{
                        Swal('Oops...', data.msg, 'error');
                    }

                },error:function(){
                    new PNotify({
                        title: 'Error',
                        text: 'Error. Please check your connection',
                        type: 'error'
                    });
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }
            });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
});
function deleteData(table, id, base){
    swal({
        title: "Apakah anda yakin untuk menghapus data ini?",
        text: "Data will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirm",
        cancelButtonText: "Cancel",
    }).then((result) => {
         if (result.value) {
         $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : base +'config/process.php', // the url where we want to POST
            data: {table: table, id_user: id, method: 'delete'},
            dataType : 'json',
            success:function(data){
                if (data.json == 'berhasil') {
                    swal("Deleted!", "Data berhasil dihapus", "success");
                    setTimeout(function(){ location.reload(); }, 500);
                }
            }
         });

     }else{

     swal("Cancelled", "You Cancelled", "error");

     }    

 });
}

function addData(){
    $('#myModal').modal('show');
}
// process the form
$('form').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    Swal(
                    'Good job!',
                    data.msg,
                    'success'
                    )
                    setTimeout(function(){ location.reload(); }, 500);
                }else{
                    Swal('Oops...', data.msg, 'error');
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
</script>