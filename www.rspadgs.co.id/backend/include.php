<style>
#header{
    border-bottom: 1px solid #c8ced3;
    min-height: 40px;
    padding: 10px 45px 10px 50px;
    margin-bottom:20px;
    background-color: #3B3F51;
    color: #fff;
}
#sidebar{
    min-height: 400px;
}

.bs-example {
    position: relative;
    padding: 45px 15px 15px;
    margin: 0 -15px 15px;
    border-color: #e5e5e5 #eee #eee;
    border-style: solid;
    border-width: 1px 0;
    -webkit-box-shadow: inset 0 3px 6px rgba(0,0,0,.05);
    box-shadow: inset 0 3px 6px rgba(0,0,0,.05);
}/*----------------sidebar-wrapper----------------*/
.sidebar-wrapper ul {
  list-style-type: none;
  padding: 0;
  margin: 0;
}
/*----------------sidebar-content----------------*/

.sidebar-content {
  overflow-y: auto;
  position: relative;
}

.sidebar-content.desktop {
  overflow-y: hidden;
}
/*----------------------sidebar-menu-------------------------*/

.sidebar-wrapper .sidebar-menu {
  padding-bottom: 10px;
}

.sidebar-wrapper .sidebar-menu .header-menu span {
  font-weight: bold;
  font-size: 14px;
  padding: 15px 20px 5px 20px;
  display: inline-block;
}

.sidebar-wrapper .sidebar-menu ul li a {
  display: inline-block;
  width: 100%;
  color: #485a6a;
  text-decoration: none;
  position: relative;
  padding: 8px 30px 15px 20px;
  font-size: 14px;
  font-weight: 400;
}

.sidebar-wrapper .sidebar-menu ul li a i {
  margin-right: 10px;
  font-size: 12px;
  width: 30px;
  height: 30px;
  line-height: 30px;
  text-align: center;
  border-radius: 4px;
}

.sidebar-wrapper .sidebar-menu ul li a:hover > i::before {
  display: inline-block;
  animation: swing ease-in-out 0.5s 1 alternate;
}

.sidebar-wrapper .sidebar-menu .sidebar-dropdown > a:after {
  font-family: "Font Awesome 5 Free";
  font-weight: 900;
  content: "\f105";
  font-style: normal;
  display: inline-block;
  font-style: normal;
  font-variant: normal;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  background: 0 0;
  position: absolute;
  right: 15px;
  top: 14px;
}
body {
    background-color: #d5d5d5 !important;
}
.sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu ul {
  padding: 5px 0;
}

.sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li {
  padding-left: 25px;
  font-size: 13px;
}

.sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a:before {
  content: "\f111";
  font-family: "Font Awesome 5 Free";
  font-weight: 400;
  font-style: normal;
  display: inline-block;
  text-align: center;
  text-decoration: none;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  margin-right: 10px;
  font-size: 8px;
}

.sidebar-wrapper .sidebar-menu ul li a span.label,
.sidebar-wrapper .sidebar-menu ul li a span.badge {
  float: right;
  margin-top: 8px;
  margin-left: 5px;
}

.sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a .badge,
.sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a .label {
  float: right;
  margin-top: 0px;
}

.sidebar-wrapper .sidebar-menu .sidebar-submenu {
  display: none;
}

.sidebar-wrapper .sidebar-menu .sidebar-dropdown.active > a:after {
  transform: rotate(90deg);
  right: 17px;
}
@media only screen and (max-width: 1000px){
 #content-page{
    margin: 15px;
} 
}
#content-page{
  background: #fff;
  padding: 20px 20px 20px 20px;
}
.thumbnail {
    display: block;
    padding: 15px 20px 20px 0px;
}
</style>
<!-- top menu -->
<div class="container-fluid" id="header">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="col-md-1 col-sm-1 col-xs-1 act" id="brand">
                <div class="pull-right">
                    <center>
                        <img src="<?php echo $base_url ?>assets/images/rspad.jpg" style="width:25px;height:auto" alt="">
                        RSPAD
                    </center>
                </div>

            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <i class="fa fa-bars fa-2x" style="margin-top:5px" onclick="actSidebar()"></i>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="pull-right" style="margin-top:5px">
            <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?php echo $_SESSION['nm_user']?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="<?php echo $base_url ?>logout.php">Log out</a></li>
            </ul>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- content -->
<div class="container">
    <div class="row">
        <!-- sidebar -->
        <div class="col-md-3 act" id="sidebar">
            <div class="thumbnail act">
                <!-- sidebar -->
                <nav id="sidebar" class="sidebar-wrapper">
                <div class="sidebar-content">            
                <div class="sidebar-menu">
                    <ul>
                    <li class="sidebar-dropdown">
                        <a href="index.php">
                        <i class="fas fa-home"></i>
                        <span >Dashboard</span>
                        </a>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="user.php">
                        <i class="fas fa-user-plus"></i>
                        <span>Data Pengguna</span>
                        </a>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="registrasi.php">
                        <i class="fas fa-database"></i>
                        <span>Data Pasien</span>
                        </a>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="rawat_jalan.php">
                        <i class="fas fa-tasks"></i>
                        <span>Rawat jalan</span>
                        </a>
                    </li>
                   <!--  <li class="sidebar-dropdown">
                        <a href="#">
                        <i class="fas fa-user-check"></i>
                        <span>Persetujuan pasien</span>
                        </a>
                    </li> -->
                    </ul>
                </div>
                <!-- sidebar-menu  -->
                </div>
                <!-- sidebar-content  -->
            </nav>
            <!-- sidebar-wrapper  -->             
            </div>
        </div>
        <!-- sidebar -->
       
