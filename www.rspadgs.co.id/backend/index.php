<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] != 'admin'){
        header("Location: ../frontend/index.php");
    } else{
//////////////////////////////////////////////////////////////////////
    include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
?>
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php
include_once('include.php');
?>
<div class="col-md-9" id="content-page">
    <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Anda berada di halaman utama admin.
    </div>
    <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12 text-center" id="left">
        <img id="img_sex" width="180"
            src="<?php echo $base_url ?>assets/images/rspad.jpg">
    </div>
    <div class="col-md-9 col-sm-6 col-xs-12">
    <table>
      <tr><td><b class="text-info">Nama User</b></td></tr>
      <tr><td><p class="text-danger"><?php echo ucwords($_SESSION['nm_user'])?></p></td></tr>
      <tr><td><b class="text-info">Posisi</b></td></tr>
      <tr><td><p class="text-danger"><?php echo ucwords($_SESSION['hak_akses'])?></p></td></tr>
      <tr><td><b class="text-info">Tanggal</b></td></tr>
      <tr><td><p class="text-danger"><?=date('m / d / Y'); ?></p></td></tr>
      <tr><td><b class="text-info">Browser</b></td></tr>
      <tr><td><p class="text-danger"><?php echo $_SERVER['HTTP_USER_AGENT'] ?></p></td></tr>
      <tr><td><b class="text-info">IP Komputer</b></td></tr>
      <tr><td><p class="text-danger"><?php echo $_SERVER['REMOTE_ADDR'];; ?></p></td></tr>
      <tr><td><b class="text-info">Nama Komputer</b></td></tr>
      <tr><td><p class="text-danger"><?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']);; ?></p></td></tr>
    </table>
    </div>
    </div>    
    </div>
  </div>
</div>

<!-- ////////////////////////////////////////////////////////////////////// -->
<?php  
?>
<?php
    include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
    }
} else { 
    header("Location: ../index.php"); // Memaksa ke halaman login jikalau bukan admin 
} 
?>