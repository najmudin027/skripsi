<?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] != 'admin'){
        header("Location: ../frontend/index.php");
    } else{
//////////////////////////////////////////////////////////////////////
    include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');

?>
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php
include_once('include.php');
?>
<div class="col-md-9" id="content-page">
        <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        Form Registrasi Pasien 
                    <div class="pull-right">
                    <a  href="cetak_pdf.php" class="btn btn-primary btn-xs" target="_blank">
                    <i class="fa fa-print" ></i> Cetak Data Pasien
                    </a>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <!-- <li class="active"><a data-toggle="tab" href="#home">Registrasi Pasien</a></li> -->
                    <li  class="active"><a data-toggle="tab" href="#menu1">Data Pasien</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade ">              
                    </div>
                    <div id="menu1" class="tab-pane fade in active">
                    <br>
<!-- ////////////////////////////////////////////////////////////////////// -->
                    <table class="table table-striped table-bordered user table-condensed">
                        <thead>
                            <tr>			
                                <th>No. RM</th>
                                <th>Nama</th>
                                <th>Tempat, Tgl Lahir</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $class = new Class_lib();
                        $sql = "SELECT * FROM registrasi";
                        $query = mysqli_query($class->conn(), $sql);
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)){ ?>
                            <tr>
                                <td><?php echo $row['id_regist'];?></td>
                                <td><?php echo $row['nm_lengkap'];?></td>
                                <td><?php echo $row['tmpt_lahir'].', '.$row['tgl_lahir'];?></td>
                                <td><?php echo $row['alamat'];?></td>

                                <td>
                                <center>
                                <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('registrasi','<?php echo $row['id_regist'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                                </center>
                                </td>
                            </tr>
                        <?php $no++; };?>
                        </tbody>
                    </table>
<!-- ////////////////////////////////////////////////////////////////////// -->
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Lihat data Pasien</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
     <div class="form-group">
        <label for="">No. RM</label>
        <p id="id_regist" name="id_regist"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Rumah Sakit</label>
        <p id="nm_rs" name="nm_rs"></p>
     </div>
     <div class="form-group">
        <label for="">Awalan Nama</label>
        <p id="id_awalan" name="id_awalan"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Lengkap</label>
        <p id="nm_lengkap" name="nm_lengkap"></p>
     </div>
     <div class="form-group">
        <label for="">Tempat Lahir</label>
        <p id="tmpt_lahir" name="tmpt_lahir"></p>
     </div>
     <div class="form-group">
        <label for="">Tanggal lahir</label>
        <p id="tgl_lahir" name="tgl_lahir"></p>
     </div>
     <div class="form-group">
        <label for="">Jenis Kelamin</label>
        <p id="jns_kelamin" name="jns_kelamin"></p>
     </div>
     <div class="form-group">
        <label for="">Alamat</label>
        <p id="alamat" name="alamat"></p>
     </div>
     <div class="form-group">
        <label for="">Kewarganegaraan</label>
        <p id="id_negara" name="id_negara"></p>
     </div>
     <div class="form-group">
        <label for="">Provinsi</label>
        <p id="id_province" name="id_province"></p>
     </div>
     <div class="form-group">
        <label for="">Kota</label>
        <p id="id_city" name="id_city"></p>
     </div>
     <div class="form-group">
        <label for="">Kabupaten</label>
        <p id="id_district" name="id_district"></p>
     </div>
     <div class="form-group">
        <label for="">Desa</label>
        <p id="id_village" name="id_village"></p>
     </div>
     <div class="form-group">
        <label for="">Suku</label>
        <p id="suku" name="suku"></p>
     </div>
     <div class="form-group">
        <label for="">Status Perkawinan</label>
        <p id="status" name="status"></p>
     </div>
     <div class="form-group">
        <label for="">Agama</label>
        <p id="agama" name="agama"></p>
     </div>

    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="form-group">
        <label for="">Telp Pasien</label>
        <p id="tlp_pasien" name="tlp_pasien"></p>
     </div>
     <div class="form-group">
        <label for="">Telp Keluarga</label>
        <p id="tlp_keluarga" name="tlp_keluarga"></p>
     </div>
     <div class="form-group">
        <label for="">Pekerjaan</label>
        <p id="id_pekerjaan" name="id_pekerjaan"></p>
     </div>
     <div class="form-group">
        <label for="">Pendidikan</label>
        <p id="id_pendidikan" name="id_pendidikan"></p>
     </div>
     <div class="form-group">
        <label for="">Golongan Darah</label>
        <p id="gol_darah" name="gol_darah"></p>
     </div>
     <div class="form-group">
        <label for="">Angkatan</label>
        <p id="id_angkatan" name="id_angkatan"></p>
     </div>
     <div class="form-group">
        <label for="">Group</label>
        <p id="id_group" name="id_group"></p>
     </div>
     <div class="form-group">
        <label for="">Golongan</label>
        <p id="id_golongan" name="id_golongan"></p>
     </div>
     <div class="form-group">
        <label for="">Status Pasien</label>
        <p id="id_stat_pasien" name="id_stat_pasien"></p>
     </div>
     <div class="form-group">
        <label for="">Nama Wakil</label>
        <p id="nm_wakil" name="nm_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Alamat Wakil</label>
        <p id="almt_wakil" name="almt_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Telp Wakil</label>
        <p id="tlp_wakil" name="tlp_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">HP Wakil</label>
        <p id="hp_wakil" name="hp_wakil"></p>
     </div>
     <div class="form-group">
        <label for="">Hubungan Keluarga</label>
        <p id="id_hub_keluarga" name="id_hub_keluarga"></p>
     </div>
     <div class="form-group">
        <label for="">Pasien Alergi</label>
        <p id="psn_alergi" name="psn_alergi"></p>
     </div>
     <div class="form-group">
        <label for="">Riwayat Penyakit</label>
        <p id="id_riwayat" name="id_riwayat"></p>
     </div>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<!-- {{-- modal edit data end--}} -->
<!-- ////////////////////////////////////////////////////////////////////// -->
<?php  
?>
<?php
    include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
    }
} else { 
    header("Location: ../index.php"); // Memaksa ke halaman login jikalau bukan admin 
} 
?>
<script>
$(document).ready(function(){
//  set datepicker
$("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'registrasi', id_regist : 'registrasi', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#id_regist').val(data.json);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
});
// stop the form from submitting the normal way and refreshing the page

// saat memilih indonesia provinsi active
$("#id_negara").change(function(){
var id_negara = $(this).val();
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'negara', id_negara : id_negara, method: 'negara'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
               $("#id_province").removeAttr("disabled");
               $('#id_province').get(0).selectedIndex = 0;

            }else{
                $('#id_province').get(0).selectedIndex = 0;
                $("select#id_province").attr('disabled','disabled');
                $('#id_city').get(0).selectedIndex = 0;
                $("select#id_city").attr('disabled','disabled');
                $('#id_district').get(0).selectedIndex = 0;
                $("select#id_district").attr('disabled','disabled');
                $('#id_village').get(0).selectedIndex = 0;
                $("select#id_village").attr('disabled','disabled');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
    });
  });
// generate cities
$("#id_province").change(function(){
    var id_province = $(this).val();
    if(id_province == ''){
        $("#id_city").attr('disabled','disabled');
        $('#id_city').get(0).selectedIndex = 0;
    } else{
        $("#id_city").removeAttr("disabled");
        $('#id_city').get(0).selectedIndex = 0;
        $("#id_district").attr('disabled','disabled');
        $('#id_district').get(0).selectedIndex = 0;
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'cities', id_province : id_province, method: 'cities'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_city').find('option').remove().end();
                    $('#id_city').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_city')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
});
// generate district
$("#id_city").change(function(){
    var id_city = $(this).val();
    if(id_city == ''){
        $("#id_district").attr('disabled','disabled');
        $('#id_district').get(0).selectedIndex = 0;
    } else{
        $("#id_district").removeAttr("disabled");
        $('#id_district').get(0).selectedIndex = 0;
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'districts', id_city : id_city, method: 'district'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_district').find('option').remove().end();
                    $('#id_district').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_district')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
});
// generate village
$("#id_district").change(function(){
    var id_district = $(this).val();
    if(id_district == ''){
        $("#id_village").attr('disabled','disabled');
        $('#id_village').get(0).selectedIndex = 0;
    } else{
        $("#id_village").removeAttr("disabled");
        $('#id_village').get(0).selectedIndex = 0;
        // process the request
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        :{table: 'villages', id_district : id_district, method: 'village'}, // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    $('#id_village').find('option').remove().end();
                    $('#id_village').append('<option value="">Pilih</option>');
                    $.each(data.json, function(key, value) {
                    $('#id_village')
                    .append('<option value="'+value.id+'">'+value.name+'</option>')  
                ;
                });
                }else{
                    Swal('Oops...', 'Error. Please check your connection', 'error')
                }

            },error:function(){
                new PNotify({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
                Swal('Oops...', 'Error. Please check your connection', 'error')
            }
        });
    }
 });
});

$("form").submit(function(e){
// process the form
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        : $('form#registrasi').serialize(), // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                Swal(
                'Good job!',
                data.msg,
                'success'
                )
                setTimeout(function(){ location.reload(); }, 1000);
            }else{
                Swal('Oops...', data.msg, 'error');
            }

        },error:function(){
            new PNotify({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
            Swal('Oops...', 'Error. Please check your connection', 'error')
        }
    });
// stop the form from submitting the normal way and refreshing the page
e.preventDefault();
});
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id_regist: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            // generate data
            $("#editModal p#id_regist").html(data.json.id_regist);
            $("#editModal p#no_rs").html(data.json.no_rs);
            $("#editModal p#id_awalan").html(data.json.awalan);
            $("#editModal p#id_negara").html(data.json.negara);
            $("#editModal p#id_province").html(data.json.provinsi);
            $("#editModal p#id_city").html(data.json.kota);
            $("#editModal p#id_district").html(data.json.kabupaten);
            $("#editModal p#id_village").html(data.json.desa);
            $("#editModal p#id_pekerjaan").html(data.json.pekerjaan);
            $("#editModal p#id_pendidikan").html(data.json.pendidikan);
            $("#editModal p#id_angkatan").html(data.json.angkatan);
            $("#editModal p#id_golongan").html(data.json.golongan);
            $("#editModal p#id_group").html(data.json.groups);
            $("#editModal p#id_stat_pasien").html(data.json.stat);
            $("#editModal p#id_riwayat").html(data.json.riwayat);
            $("#editModal p#id_hub_keluarga").html(data.json.hub);
            // dari db
            $("#editModal p#nm_rs").html(data.json.nm_rs);
            $("#editModal p#nm_lengkap").html(data.json.nm_lengkap);
            $("#editModal p#tmpt_lahir").html(data.json.tmpt_lahir);
            $("#editModal p#tgl_lahir").html(data.json.tgl_lahir);
            $("#editModal p#jns_kelamin").html(data.json.jns_kelamin);
            $("#editModal p#alamat").html(data.json.alamat);
            $("#editModal p#suku").html(data.json.suku);
            $("#editModal p#status").html(data.json.status);
            $("#editModal p#agama").html(data.json.agama);
            $("#editModal p#tlp_pasien").html(data.json.tlp_pasien);
            $("#editModal p#tlp_keluarga").html(data.json.tlp_keluarga);
            $("#editModal p#gol_darah").html(data.json.gol_darah);
            $("#editModal p#nm_wakil").html(data.json.nm_wakil);
            $("#editModal p#almt_wakil").html(data.json.almt_wakil);
            $("#editModal p#tlp_wakil").html(data.json.tlp_wakil);
            $("#editModal p#hp_wakil").html(data.json.hp_wakil);
            $("#editModal p#psn_alergi").html(data.json.psn_alergi);

        }
    }
    });
}
</script>