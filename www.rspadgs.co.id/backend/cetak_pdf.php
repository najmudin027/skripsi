<?php
session_start();
if(isset($_SESSION['id_user']) != true){
  header("Location: ../index.php");
} else{ 
  // include_once('../../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
  // memanggil library FPDF
  require('fpdf/fpdf.php');
  // intance object dan memberikan pengaturan halaman PDF
  $pdf = new FPDF('P','mm','A4');
  // membuat halaman baru
  $pdf->AddPage();
  // setting jenis font yang akan digunakan
  $pdf->SetFont('Arial','B',16);
  // mencetak string 
  $pdf->SetFont('Arial','B',14);
  $pdf->Cell(190,6,'Pelayanan instalasi gawat darurat pada rumah sakit rspad gatot subroto',0,1,'C');
  $pdf->SetFont('Arial','B',12);
  $pdf->Cell(190,6,'Alamat: Jl. Abdul Rahman Saleh No. 24, Jakarta Pusat 10410,  ',0,1,'C');
  $pdf->Cell(190,6,'Phone : 021 - 3441008, 3840702 Fax. 021 - 3520619.',0,1,'C');
  $pdf->Cell(190,6,'(Laporan Data Registrasi)',0,1,'C');

  // Memberikan space kebawah agar tidak terlalu rapat
  $pdf->Cell(10,7,'',0,1);
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(25,6,'ID Registrasi',1,0);
  $pdf->Cell(35,6,'Nama Lengkap',1,0);
  $pdf->Cell(35,6,'Tempat, Tgl Lahir',1,0);
  $pdf->Cell(20,6,'Gol Darah',1,0);
  $pdf->Cell(35,6,'Telp. pasien',1,0);
  $pdf->Cell(35,6,'Nama Rumah Sakit',1,1);
  // $pdf->Cell(35,6,'Nama Wakil',1,0);
  // $pdf->Cell(35,6,'Telp. Wakil',1,0);
  // $pdf->Cell(32,6,'Alamat Wakil',1,1);

  $pdf->SetFont('Arial','',10);
  $conn = mysqli_connect($host = 'localhost', $username = 'root', $password ='', $database = 'db_rspad');
  $sql = mysqli_query($conn, "SELECT * FROM registrasi ORDER BY id_regist ASC");  
  $i   = 1;
  while($row=mysqli_fetch_array($sql)){
      $pdf->Cell(25,6,$row['id_regist'],1,0);
      $pdf->Cell(35,6,$row['nm_lengkap'],1,0);
      $pdf->Cell(35,6,strtolower($row['tmpt_lahir']).', '.$row['tgl_lahir'],1,0);
      $pdf->Cell(20,6,strtoupper($row['gol_darah']),1,0);
      $pdf->Cell(35,6,$row['tlp_pasien'],1,0);
      $pdf->Cell(35,6,$row['nm_rs'],1,1);
      // $pdf->Cell(35,6,$row['nm_wakil'],1,0);
      // $pdf->Cell(35,6,$row['tlp_wakil'],1,0);
      // $pdf->Cell(32,6,$row['almt_wakil'],1,1); 
  }
$pdf->Output();

 }
 ?>