<?php
    
class Class_lib{
function __construct(){
    // cek koneksi
    if ($this->conn()) {
        if(mysqli_connect_error()){
            echo "Error: " . mysqli_connect_error();
            exit();
        }
    }
}

/**
 * Fungsi koneksi database.
 */
function conn($host = 'localhost', $username = 'root', $password ='', $database = 'db_rspad')
{
    $conn = mysqli_connect($host, $username, $password, $database);
    // Menampilkan pesan error jika database tidak terhubung
    return ($conn) ? $conn : "Koneksi database gagal: " . mysqli_connect_error();
}

function login($arr = ''){
    $sql 	= 'SELECT * FROM user where id_user = \''.$arr['id_user'].'\' AND password = \''.md5($arr['password']).'\'';
    $query 	= mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $fetch = mysqli_fetch_array($query);
        $sess = $this->session($fetch);
        echo json_encode(array('error'=>0,'json'=> $fetch));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function edit($arr = ''){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    if($arr['table'] !='registrasi'){
        $sql 	= 'SELECT * FROM '.$arr['table'].' where '. $keyArr[1].' = \''.$valArr[1].'\' ';
    } elseif ($arr['table'] == 'registrasi') {
        $sql = 'SELECT  r.*,a.`awalan` AS awalan, n.`nm_negara` AS negara,
                p.`name` AS provinsi, c.`name` AS kota, d.`name` AS kabupaten,
                v.`name` AS desa, pk.`nm_pekerjaan` AS pekerjaan, pn.`nm_pendidikan` AS pendidikan,
                an.`nm_angkatan` AS angkatan, gr.`nm_group` AS groups, go.`nm_golongan` AS golongan,
                st.`nm_status` AS stat, rw.`nm_penyakit` AS riwayat,
                hu.`nm_status` AS hub
                FROM registrasi AS r 
                LEFT JOIN `nm_awalan` AS a ON r.`id_awalan` = a.`id`
                LEFT JOIN negara AS n ON r.`id_negara` = n.`id`
                LEFT JOIN provinces AS p ON  r.`id_province` = p.`id`
                LEFT JOIN cities AS c ON r.`id_city` = c.`id`
                LEFT JOIN districts AS d ON r.`id_district` = d.`id`
                LEFT JOIN villages AS v ON r.`id_village` = v.`id`
                LEFT JOIN pekerjaan AS pk ON r.`id_pekerjaan` = pk.`id`
                LEFT JOIN pendidikan AS pn ON r.`id_pendidikan` = pn.`id`
                LEFT JOIN angkatan AS an ON r.`id_angkatan` = an.`id`
                LEFT JOIN group_pasien AS gr ON r.`id_group` = gr.`id`
                LEFT JOIN golongan AS go ON r.`id_golongan` = go.`id`
                LEFT JOIN stat_pasien AS st ON r.`id_stat_pasien` = st.`id`
                LEFT JOIN riwayat_penyakit AS rw ON r.`id_riwayat` = rw.`id`
                LEFT JOIN stat_pasien AS hu ON r.`id_hub_keluarga` = hu.`id`
                where r.id_regist = \''.$valArr[1].'\'';
    }    
    if ($arr['table'] == 'rawat_jalan') {
        $sql = 'SELECT  r.*, gr.`nm_group` AS groups, go.`nm_golongan` AS golongan
                FROM rawat_jalan AS r 
                LEFT JOIN group_pasien AS gr ON r.`id_group` = gr.`id`
                LEFT JOIN golongan AS go ON r.`id_golongan` = go.`id`
                where r.no_rawat = \''.$valArr[1].'\'';
    }
    $query 	= mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $fetch = mysqli_fetch_array($query);
        echo json_encode(array('error'=>0,'msg'=> 'berhasil', 'json' => $fetch));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function update_data($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $key = $this->getKeysArr($arr);
    $valueSets = array();
    $ex_field = array();
    
    foreach ($key as $e) {
        $cekField = 'SHOW COLUMNS FROM '.$arr['edit_data'].' WHERE FIELD = \''.$e.'\'';
        $query 	= mysqli_query($this->conn(), $cekField);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            array_push($ex_field, $e);
            if($e == 'password'){
                $valueSets[] = $e . " = '" . md5($arr[$e]) . "'";
            } else if($arr['edit_data'] == 'rawat_jalan' && $arr[$e] == 'Ditolak'){
                $valueSets[] = $e . " = '" . $arr[$e] . "'";
                $valueSets[] = 'status = "CLOSE"';
            } else{
                $valueSets[] = $e . " = '" . $arr[$e] . "'";
            }
        }
    }
    $insert = 'update '.$arr['edit_data'].' set  '.join(",",$valueSets).' where '.$valArr[1].' = \''.$arr['id_data'].'\'';
    $query 	= mysqli_query($this->conn(), $insert);
    if($query){
        echo json_encode(array('error'=>0,'msg'=> 'Data berhasil diperbaharui'));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function delete($arr = null)
{
    $key = $this->getKeysArr($arr);
    $ex_field = implode( ", ", $key);
    foreach ($key as $e) {
        $cekField = 'SHOW COLUMNS FROM '.$arr['table'].' WHERE FIELD = \''.$e.'\'';
        $query 	= mysqli_query($this->conn(), $cekField);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            $sql 	= 'delete from '.$arr['table'].' where '.$e.' = \''.$arr[$e].'\'';
            $query 	= mysqli_query($this->conn(), $sql);
            if($query){
                echo json_encode(array('error'=>0,'json'=> 'berhasil'));
            } else{
                echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
            }
        }
    }   
}
function add_data($arr = null)
{
    $key = $this->getKeysArr($arr);
    $ex_field = array();
    $ex_value = array();
    foreach ($key as $e) {
        $cekField = 'SHOW COLUMNS FROM '.$arr['add_data'].' WHERE FIELD = \''.$e.'\'';
        $query 	= mysqli_query($this->conn(), $cekField);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            array_push($ex_field, $e);
            if($e == 'password'){
                array_push($ex_value,  '"'.md5($arr[$e]).'"');
            } elseif($e == 'tgl_lahir' || $e == 'tgl_regist'){
                $d = explode('-',$arr[$e]);
                $tgl_lahir = $d[2].'-'.$d[1].'-'.$d[0];
                array_push($ex_value, '"'.$tgl_lahir.'"');
            } else{
                array_push($ex_value, '"'.$arr[$e].'"');
            }

        }
    }
    $ex_field = implode( ", ", $ex_field);
    $ex_value = implode( ", ", $ex_value);
    $insert = 'insert into '.$arr['add_data'].' ('.$ex_field.') values('.$ex_value.')';
    if($arr['add_data'] == 'rawat_jalan'){
        $check = 'SELECT * FROM rawat_jalan where id_regist =\''.$arr['id_regist'].'\' AND status ="open"';
        $ck = mysqli_query($this->conn(), $check);
        $rowcount = mysqli_num_rows($ck);
        if($rowcount){
            echo json_encode(array('error'=>1,'msg'=>'Pasien telah terdaftar dan rawat jalan masih berlaku. '));  
            exit(); 
        } else{
            $query 	= mysqli_query($this->conn(), $insert);
         echo json_encode(array('error'=>0,'msg'=> 'Data berhasil ditambahkan'));

        }
    }else{
        $query 	= mysqli_query($this->conn(), $insert);
         echo json_encode(array('error'=>0,'msg'=> 'Data berhasil ditambahkan'));
    }
    // if($query){
    //     echo json_encode(array('error'=>0,'msg'=> 'Data berhasil ditambahkan'));
    // } else{
    //     echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    // }
}
function genUser($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    if($valArr[1] == 'admin'){
        $sql = "SELECT max(`id_user`) as id_user FROM user where left(id_user, 3) = 'ADM'";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['id_user'] == null or $getId['id_user'] == ''){
                $id_user = "ADM0001";
            }else{
                $id_user = (int) substr($getId['id_user'], 3, 6)+1;
                $char = "ADM";
                $id_generate = $char . sprintf("%04s", $id_user);
            }

    } else if($valArr[1] == 'petugas'){
        $sql = "SELECT max(`id_user`) as id_user FROM user where left(id_user, 3) = 'PTG'";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['id_user'] == null or $getId['id_user'] == ''){
                $id_user = "PTG0001";
            }else{
                $id_user = (int) substr($getId['id_user'], 3, 6)+1;
                $char = "PTG";
                $id_generate = $char . sprintf("%04s", $id_user);
            }
    } else if($valArr[1] == 'registrasi'){
        $id_generate = date('ymdhms');
    } else if($valArr[1] == 'rawat'){
        $sql = "SELECT max(`no_rawat`) as no_rawat FROM rawat_jalan";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['no_rawat'] == null or $getId['no_rawat'] == ''){
                $id_generate = "RJ".date('ymd')."00001";
            }else{
                $no_rawat = (int) substr($getId['no_rawat'], 8, 6)+1;
                $char = "RJ".date('ymd');
                $id_generate = $char . sprintf("%05s", $no_rawat);
            }
    }
    echo json_encode(array('error'=>0,'msg'=> 'Data berhasil ditambahkan', 'json' => $id_generate));
    
}

function negara($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $sql = 'select *from '.$arr['table'].' WHERE id = \''.$valArr[1].'\' limit 1';
    $query = mysqli_query($this->conn(), $sql);
    $ft = mysqli_fetch_array($query);
    if($ft['nm_negara'] == 'Indonesia'){
        $sql = 'select *from provinces';
        $query = mysqli_query($this->conn(), $sql);
        echo json_encode(array('error'=>0,'msg'=> 'tersedia'));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}

function cities($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $sql = 'select *from '.$arr['table'].' WHERE province_id = \''.$valArr[1].'\'';
    $query = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $responsistem = array();
        $responsistem["data"] = array();
        //fetech all data from json table in associative array format and store in $result variable
        while ($row = mysqli_fetch_assoc($query)) {
            $data['id'] = $row["id"];
            $data['province_id'] = $row["province_id"];
            $data['name'] = $row["name"];
            array_push($responsistem["data"], $data);
        }

        echo json_encode(array('error'=>0,'msg'=> 'tersedia', 'json'=> $responsistem["data"]));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}

function district($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $sql = 'select *from '.$arr['table'].' WHERE city_id = \''.$valArr[1].'\'';
    $query = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $responsistem = array();
        $responsistem["data"] = array();
        //fetech all data from json table in associative array format and store in $result variable
        while ($row = mysqli_fetch_assoc($query)) {
            $data['id'] = $row["id"];
            $data['city_id'] = $row["city_id"];
            $data['name'] = $row["name"];
            array_push($responsistem["data"], $data);
        }
        
        
        //Now encode PHP array in JSON string 
        echo json_encode(array('error'=>0,'msg'=> 'tersedia', 'json'=> $responsistem["data"]));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}

function village($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $sql = 'select *from '.$arr['table'].' WHERE district_id = \''.$valArr[1].'\'';
    $query = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $responsistem = array();
        $responsistem["data"] = array();
        //fetech all data from json table in associative array format and store in $result variable
        while ($row = mysqli_fetch_assoc($query)) {
            $data['id'] = $row["id"];
            $data['district_id'] = $row["district_id"];
            $data['name'] = $row["name"];
            array_push($responsistem["data"], $data);
        }
        
        
        //Now encode PHP array in JSON string 
        echo json_encode(array('error'=>0,'msg'=> 'tersedia', 'json'=> $responsistem["data"]));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}

function getKeysArr($dA='')
{
    $i   	    = 0;
    $key_ready  = array();
    foreach ($dA as $key => $value) {
        if(is_string($key)){
            // if($value[$i] == '' || $value[$i] == null ){
            //     continue;
            // } else {
                $key_ready [] = $key;
            // }
        }
    }
    return $key_ready;
    
}
function session($arr){
    session_start();
    $key = $this->getKeysArr($arr);
    foreach ($key as $e) {
        $_SESSION[$e]=$arr[$e];
    }
}

}
