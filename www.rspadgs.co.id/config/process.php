<?php
    include_once('Class_lib.php');
    $class = new Class_lib();
    
    // handle prosess login
    if(isset($_POST['login'])){
        $check = $class->login($_POST);
    }

    // handle prosess add data
    if(isset($_POST['add_data'])){
        $check = $class->add_data($_POST);
    }

    if(isset($_POST['method'])){
        // handle prosess edit data
        if($_POST['method'] == 'edit'){
            $check = $class->edit($_POST);
        }
        // handle prosess delete data
        if($_POST['method'] == 'delete'){
            $check = $class->delete($_POST);
        }
        // handle prosess generate data
        if($_POST['method'] == 'generate'){
            $check = $class->genUser($_POST);
        }
        // handle prosess negara
        if($_POST['method'] == 'negara'){
            $check = $class->negara($_POST);
        }
        // handle prosess negara
        if($_POST['method'] == 'cities'){
            $check = $class->cities($_POST);
        }
        // handle prosess negara
        if($_POST['method'] == 'district'){
            $check = $class->district($_POST);
        }
        // handle prosess negara
        if($_POST['method'] == 'village'){
            $check = $class->village($_POST);
        }

    }

    // handle prosess update data
    if(isset($_POST['id_data'])){
        $check = $class->update_data($_POST);
    }

?>