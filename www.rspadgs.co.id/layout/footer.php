	<div class="container">
        2019 © RSPAD Gatot Subroto.
    </div>
    <!-- This for javascript external -->
    <script src="<?php echo $base_url ?>assets/jquery-3.4.1.js"></script>
    <script src="<?php echo $base_url ?>assets/sweetalert2.all.js"></script>
    <script src="<?php echo $base_url ?>assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $base_url ?>assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url ?>assets/fontawesome-free-5.5.0/js/all.min.js"></script>
    <script src="<?php echo $base_url ?>assets/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="<?php echo $base_url ?>assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $base_url ?>assets/datatables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $base_url ?>assets/custom.js"></script>
</body>
</html>