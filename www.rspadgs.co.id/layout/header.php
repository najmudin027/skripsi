<?php
    $base_url = 'http://localhost/skripsi/www.rspadgs.co.id/';
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pelayanan instalasi gawat darurat pada rumah sakit rspad gatot subroto</title>
    <!-- This is for icon website -->
    <link rel="shortcut icon" href="<?php echo $base_url ?>assets/images/favicon.ico">
    <!-- This is for package css external -->
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/custom.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/fontawesome-free-5.5.0/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/datatables/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/jquery-ui.css">
</head>