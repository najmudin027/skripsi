/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.24 : Database - db_ikgk
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_ikgk` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_ikgk`;

/*Table structure for table `anggota` */

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id_anggota` char(20) NOT NULL,
  `nm_lengkap` char(40) DEFAULT NULL,
  `jns_kelamin` enum('Perempuan','Laki-Laki') DEFAULT 'Perempuan',
  `tmpt_lahir` char(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_ktp` char(20) DEFAULT NULL,
  `alamat` char(60) DEFAULT NULL,
  `no_telp` char(15) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `tgl_bergabung` date DEFAULT NULL,
  `keanggotaan` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `nip` char(20) DEFAULT NULL,
  PRIMARY KEY (`id_anggota`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `anggota` */

insert  into `anggota`(`id_anggota`,`nm_lengkap`,`jns_kelamin`,`tmpt_lahir`,`tgl_lahir`,`no_ktp`,`alamat`,`no_telp`,`id_jabatan`,`id_status`,`tgl_bergabung`,`keanggotaan`,`nip`) values 
('0001/IKGK/19','Duma Indah Sari,M.Pd','Laki-Laki','Pematang Siantar','2019-06-29','31730626739704','medan','08111503333',7,3,'2019-06-29','aktif','12164578'),
('0002/IKGK/19','DIT spd','Perempuan','bogor','2019-06-29','12345678910','bogor','08767789917',5,3,'2019-06-29','aktif','12164578'),
('0003/IKGK/19','SITI AISYAH','Perempuan','JAKARTA','1990-04-18','31747575757575755','JL KALI DERES','08111503333',5,3,'2018-06-29','aktif','196304071985032009');

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jabatan` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `jabatan` */

insert  into `jabatan`(`id`,`nm_jabatan`) values 
(5,'Pengajar'),
(6,'Humas'),
(7,'Kepala Sekolah'),
(8,'Kesiswaan'),
(9,'Bimbingan dan Konseling');

/*Table structure for table `pembayaran_pinjaman` */

DROP TABLE IF EXISTS `pembayaran_pinjaman`;

CREATE TABLE `pembayaran_pinjaman` (
  `no_pembayaran` char(15) NOT NULL,
  `id_anggota` char(15) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  PRIMARY KEY (`no_pembayaran`),
  KEY `id_anggota` (`id_anggota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pembayaran_pinjaman` */

insert  into `pembayaran_pinjaman`(`no_pembayaran`,`id_anggota`,`nominal`,`tgl_bayar`) values 
('1906-BAYAR-0001','0001/IKGK/19',500000,'2019-06-29'),
('1906-BAYAR-0002','0002/IKGK/19',1000000,'2019-06-29'),
('1906-BAYAR-0003','0003/IKGK/19',500000,'2019-06-29');

/*Table structure for table `pencairan_simpanan` */

DROP TABLE IF EXISTS `pencairan_simpanan`;

CREATE TABLE `pencairan_simpanan` (
  `no_pencairan` char(15) NOT NULL,
  `id_anggota` char(15) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tgl_cair` date DEFAULT NULL,
  PRIMARY KEY (`no_pencairan`),
  KEY `id_anggota` (`id_anggota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pencairan_simpanan` */

insert  into `pencairan_simpanan`(`no_pencairan`,`id_anggota`,`nominal`,`tgl_cair`) values 
('1906-CAIR-0001','0001/IKGK/19',10000,'2019-06-29'),
('1906-CAIR-0002','0003/IKGK/19',50000,'2019-06-29');

/*Table structure for table `pinjaman` */

DROP TABLE IF EXISTS `pinjaman`;

CREATE TABLE `pinjaman` (
  `no_pinjaman` char(15) NOT NULL,
  `id_anggota` char(15) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `status` enum('lunas','belum lunas') DEFAULT 'belum lunas',
  PRIMARY KEY (`no_pinjaman`),
  KEY `id_anggota` (`id_anggota`),
  CONSTRAINT `pinjaman_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pinjaman` */

insert  into `pinjaman`(`no_pinjaman`,`id_anggota`,`nominal`,`tgl_pinjaman`,`status`) values 
('1906-PJ-0001','0001/IKGK/19',1000000,'2019-06-29','belum lunas'),
('1906-PJ-0002','0002/IKGK/19',10000000,'2019-06-29','belum lunas'),
('1906-PJ-0003','0003/IKGK/19',5000000,'2018-06-07','belum lunas');

/*Table structure for table `riwayat_pinjaman` */

DROP TABLE IF EXISTS `riwayat_pinjaman`;

CREATE TABLE `riwayat_pinjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_anggota` char(20) DEFAULT NULL,
  `nominal_in` int(11) DEFAULT '0',
  `nominal_out` int(11) DEFAULT '0',
  `hutang_akhir` int(11) DEFAULT '0',
  `keperluan` char(20) DEFAULT NULL,
  `no_pinjaman` char(15) DEFAULT NULL,
  `no_pembayaran` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_anggota` (`id_anggota`),
  KEY `no_pinjaman` (`no_pinjaman`),
  KEY `no_pembayaran` (`no_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `riwayat_pinjaman` */

insert  into `riwayat_pinjaman`(`id`,`tanggal`,`id_anggota`,`nominal_in`,`nominal_out`,`hutang_akhir`,`keperluan`,`no_pinjaman`,`no_pembayaran`) values 
(1,'2019-06-29','0001/IKGK/19',0,1000000,1000000,'w','1906-PJ-0001',NULL),
(2,'2019-06-29','0001/IKGK/19',500000,0,500000,'pembayaran',NULL,'1906-BAYAR-0001'),
(3,'2019-06-29','0002/IKGK/19',0,10000000,10000000,'pembayaran kuliah an','1906-PJ-0002',NULL),
(4,'2019-06-29','0002/IKGK/19',1000000,0,9000000,'pembayaran',NULL,'1906-BAYAR-0002'),
(5,'2018-06-07','0003/IKGK/19',0,5000000,5000000,'KEPERLUAN PRIBADI','1906-PJ-0003',NULL),
(6,'2019-06-29','0003/IKGK/19',500000,0,4500000,'pembayaran',NULL,'1906-BAYAR-0003');

/*Table structure for table `riwayat_transaksi` */

DROP TABLE IF EXISTS `riwayat_transaksi`;

CREATE TABLE `riwayat_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_anggota` char(20) DEFAULT NULL,
  `nominal_in` int(11) DEFAULT '0',
  `nominal_out` int(11) DEFAULT '0',
  `nominal_akhir` int(11) DEFAULT '0',
  `keperluan` char(20) DEFAULT NULL,
  `no_simpanan` char(15) DEFAULT NULL,
  `no_pencairan` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `no_simpanan` (`no_simpanan`),
  KEY `no_pencairan` (`no_pencairan`),
  KEY `id_anggota` (`id_anggota`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `riwayat_transaksi` */

insert  into `riwayat_transaksi`(`id`,`tanggal`,`id_anggota`,`nominal_in`,`nominal_out`,`nominal_akhir`,`keperluan`,`no_simpanan`,`no_pencairan`) values 
(1,'2019-06-29','0001/IKGK/19',100000,0,100000,'simpanan','1906-SP-0001',NULL),
(2,'2019-06-29','0001/IKGK/19',0,10000,90000,'pencairan',NULL,'1906-CAIR-0001'),
(3,'2019-06-29','0003/IKGK/19',500000,0,500000,'simpanan','1906-SP-0002',NULL),
(4,'2019-06-29','0003/IKGK/19',0,50000,450000,'pencairan',NULL,'1906-CAIR-0002');

/*Table structure for table `simpanan` */

DROP TABLE IF EXISTS `simpanan`;

CREATE TABLE `simpanan` (
  `no_simpanan` char(15) NOT NULL,
  `id_anggota` char(15) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tgl_setor` date DEFAULT NULL,
  PRIMARY KEY (`no_simpanan`),
  KEY `id_anggota` (`id_anggota`),
  CONSTRAINT `simpanan_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `simpanan` */

insert  into `simpanan`(`no_simpanan`,`id_anggota`,`nominal`,`tgl_setor`) values 
('1906-SP-0001','0001/IKGK/19',100000,'2019-06-29'),
('1906-SP-0002','0003/IKGK/19',500000,'2019-06-29');

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_status` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

insert  into `status`(`id`,`nm_status`) values 
(2,'Honorer'),
(3,'PNS');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_user` char(12) NOT NULL,
  `nm_user` char(50) DEFAULT NULL,
  `no_telp` char(15) DEFAULT NULL,
  `alamat` varchar(60) DEFAULT NULL,
  `username` char(20) DEFAULT NULL,
  `password` char(60) DEFAULT NULL,
  `hak_akses` enum('admin','kepala') DEFAULT 'admin',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id_user`,`nm_user`,`no_telp`,`alamat`,`username`,`password`,`hak_akses`) values 
('IKGK0001','Rio Fernandes','4365476766','jkt','rio','e10adc3949ba59abbe56e057f20f883e','admin'),
('IKGK0002','Nency Yunica','08119872602','Jl.Percetakan Negara III Rt.005 /RW 001','nency06','e10adc3949ba59abbe56e057f20f883e','admin'),
('KPLA0001','Joko Anwar','08119872602','Jl.Percetakan Negara III Rt.005 /RW 001','kepala','e10adc3949ba59abbe56e057f20f883e','kepala');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
