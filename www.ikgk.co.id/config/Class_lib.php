<?php
    
class Class_lib{
function __construct(){
    // cek koneksi
    if ($this->conn()) {
        if(mysqli_connect_error()){
            echo "Error: " . mysqli_connect_error();
            exit();
        }
    }
}

/**
 * Fungsi koneksi database.
 */
function conn($host = 'localhost', $username = 'root', $password ='', $database = 'db_ikgk')
{
    $conn = mysqli_connect($host, $username, $password, $database);
    // Menampilkan pesan error jika database tidak terhubung
    return ($conn) ? $conn : "Koneksi database gagal: " . mysqli_connect_error();
}

function login($arr = ''){
    $sql = 'SELECT * FROM users where (id_user = \''.$arr['id_user'].'\' OR username = \''.$arr['id_user'].'\') AND password = \''.md5($arr['password']).'\'';
    $query  = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        $fetch = mysqli_fetch_array($query);
        $sess = $this->session($fetch);
        echo json_encode(array('error'=>0,'json'=> $fetch));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function edit($arr = ''){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $pk = $this->pkName($arr);
    if ($arr['table'] == 'simpanan' || $arr['table'] == 'pencairan_simpanan' || $arr['table'] == 'pinjaman' || $arr['table'] == 'pembayaran_pinjaman' || $arr['table'] == 'riwayat_transaksi' || $arr['table'] == 'riwayat_pinjaman') {
        $sql = 'SELECT * FROM '.$arr['table'].' where '. $keyArr[1].' = \''.trim($valArr[1]).'\'';
    } elseif($arr['table'] == 'riwayat_transaksi'){
        $sql = 'SELECT * FROM riwayat_transaksi where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id desc limit 1';
    } elseif($arr['table'] == 'riwayat_pinjaman'){
        $sql = 'SELECT * FROM riwayat_pinjaman where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id desc limit 1';
    } elseif($arr['table'] != 'simpanan'){
        $sql = 'SELECT * FROM '.$arr['table'].' where '. $pk.' = \''.trim($valArr[1]).'\' ';
    } 
    $query = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
        if ($arr['table'] == 'simpanan' || $arr['table'] == 'pencairan_simpanan' || $arr['table'] == 'pinjaman' || $arr['table'] == 'pembayaran_pinjaman' || $arr['table'] == 'riwayat_transaksi' || $arr['table'] == 'riwayat_pinjaman') {
            $fetch = mysqli_fetch_all( $query);
        } elseif($arr['table'] == 'riwayat_transaksi'){
            $fetch = mysqli_fetch_array($query);
        } elseif($arr['table'] == 'riwayat_pinjaman'){
            $fetch = mysqli_fetch_array($query);
        } elseif($arr['table'] != 'simpanan'){
            $fetch = mysqli_fetch_array($query);
        } 
            echo json_encode(array('error'=>0,'msg'=> 'berhasil', 'json' => $fetch));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
public function getEdit($arr='')
{
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $pk = $this->pkName($arr);
    $sql = 'SELECT * FROM '.$arr['table'].' where '. $pk.' = \''.trim($valArr[1]).'\' ';
    $query = mysqli_query($this->conn(), $sql);
    $rowcount = mysqli_num_rows($query);
    if($rowcount){
         $fetch = mysqli_fetch_array($query);
         echo json_encode(array('error'=>0,'msg'=> 'berhasil', 'json' => $fetch));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function update_data($arr = null){
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $key = $this->getKeysArr($arr);
    $valueSets = array();
    $ex_field = array();
    foreach ($key as $e) {
        $cekField = 'SHOW COLUMNS FROM '.$arr['edit_data'].' WHERE FIELD = \''.$e.'\'';
        $query  = mysqli_query($this->conn(), $cekField);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            array_push($ex_field, $e);
            if($e == 'password'){
                if ($arr[$e] != '') {
                    $valueSets[] = $e . " = '" . md5($arr[$e]) . "'";
                }
            } elseif($e == 'tgl_lahir' || $e == 'tgl_bergabung' || $e == 'tgl_setor'){
                $d = explode('-',$arr[$e]);
                $tgl_lahir = $d[2].'-'.$d[1].'-'.$d[0];
                $valueSets[] = $e . " = '" . $tgl_lahir . "'";
            } else{
                $valueSets[] = $e . " = '" . $arr[$e] . "'";
            }
        }
    }
    $insert = 'update '.$arr['edit_data'].' set  '.join(",",$valueSets).' where '.trim($valArr[1]).' = \''.$arr['id_data'].'\'';
    $query  = mysqli_query($this->conn(), $insert);
    if($query){
        echo json_encode(array('error'=>0,'msg'=> 'Data berhasil diperbaharui'));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }
}
function delete($arr = null)
{
    $pk = $this->pkName($arr);
    $sql    = 'Delete from '.$arr['table'].' where '.$pk.' = \''.$arr['field'].'\'';
    $query  = mysqli_query($this->conn(), $sql);
    if($query){
        echo json_encode(array('error'=>0,'json'=> 'Data berhasil dihapus.'));
    } else{
        echo json_encode(array('error'=>1,'msg'=>'Data yang anda masukkan tidak cocok dengan database kami. '));        
    }

}
public function pkName($arr = null)
{
    $cekField = 'SHOW KEYS FROM '.$arr['table'].' WHERE Key_name = "PRIMARY"';
    $query  = mysqli_query($this->conn(), $cekField);
    $ft = mysqli_fetch_array($query);
    return $ft['Column_name'];
}
function add_data($arr = null)
{
    $key = $this->getKeysArr($arr);
    $ex_field = array();
    $ex_value = array();
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    foreach ($key as $e) {
        $cekField = 'SHOW COLUMNS FROM '.$arr['add_data'].' WHERE FIELD = \''.$e.'\'';
        $query  = mysqli_query($this->conn(), $cekField);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            array_push($ex_field, $e);
            if($e == 'password'){
                array_push($ex_value,  '"'.md5($arr[$e]).'"');
            } elseif($e == 'tgl_lahir' || $e == 'tgl_bergabung' || $e == 'tgl_setor' || $e == 'tgl_cair' || $e == 'tgl_pinjaman' || $e == 'tgl_bayar'){
                $d = explode('-',$arr[$e]);
                $tgl_lahir = $d[2].'-'.$d[1].'-'.$d[0];
                array_push($ex_value, '"'.$tgl_lahir.'"');
            } else{
                array_push($ex_value, '"'.$arr[$e].'"');
            }

        }
    }
    if($arr['add_data'] == 'simpanan'){
        $this->log_transaksi($arr, 'simpanan', $ex_field, $ex_value);
    } elseif($arr['add_data'] == 'pencairan_simpanan'){
        $this->log_transaksi($arr, 'pencairan', $ex_field, $ex_value);
    } elseif($arr['add_data'] == 'pinjaman'){
        // peraturan saat melakukan pinjaman
        $cekPinjam = 'SELECT * FROM '.$arr['add_data'].' where '. $keyArr[1].' = \''.trim($valArr[1]).'\' and status = "belum lunas"';
        $qryPjm  = mysqli_query($this->conn(), $cekPinjam);
        $rowPjm = mysqli_num_rows($qryPjm);
        if ($rowPjm) {
             echo json_encode(array('error'=>1,'msg'=>'Anggota masih memiliki tanggungan untuk membayar pinjaman yang belum lunas. '));
             exit();
        }

        $cekStatus = 'SELECT * FROM anggota where '. $keyArr[1].' = \''.trim($valArr[1]).'\' limit 1';
        $qryStat  = mysqli_query($this->conn(), $cekStatus);
        $rowStat = mysqli_num_rows($qryStat);
        if ($rowStat) {
            $fetch = mysqli_fetch_array($qryStat);
            $cekPns = 'SELECT * FROM status where id_status = \''.$fetch['id_status'].'\' limit 1';
            $qryPns  = mysqli_query($this->conn(), $cekPns);
            $rowPns = mysqli_num_rows($qryPns);
            if ($rowPns) {
                $takeIt = mysqli_fetch_array($qryPns);
                    if($takeIt['nm_status'] == 'PNS' || $takeIt['nm_status'] == 'pns' || $takeIt['nm_status'] == 'pegawai negeri sipil'){
                    }else{
                         echo json_encode(array('error'=>1,'msg'=>'Anggota masih berstatus bukan sebagai pegawai negeri sipil. '));
                         exit();
                    }
            }
        }
        $this->log_pinjaman($arr, 'pinjaman', $ex_field, $ex_value);
    } elseif($arr['add_data'] == 'pembayaran_pinjaman'){
        $this->log_pinjaman($arr, 'pembayaran', $ex_field, $ex_value);

    }
    $ex_field = implode( ", ", $ex_field);
    $ex_value = implode( ", ", $ex_value);
    $insert = 'insert into '.$arr['add_data'].' ('.$ex_field.') values('.$ex_value.')';
    $query  = mysqli_query($this->conn(), $insert);
    echo json_encode(array('error'=>0,'msg'=> 'Data berhasil ditambahkan'));
}
public function log_pinjaman($arr='', $log, $field, $val)
{
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    if($log == 'pinjaman'){
        $sql = 'SELECT * FROM riwayat_pinjaman where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id_pinjaman desc limit 1';
        $query = mysqli_query($this->conn(), $sql);
        $rowcount = mysqli_num_rows($query);
        if(!$rowcount){
            $qr = 'insert into riwayat_pinjaman (tanggal, id_anggota, nominal_out, hutang_akhir, keperluan, no_pinjaman) values ('.$val[3].', \''.trim($valArr[1]).'\', \''.$valArr[3].'\', \''.$valArr[3].'\', \''.$valArr[5].'\', \''.$arr['no_pinjaman'].'\')';
            $query = mysqli_query($this->conn(), $qr);
        }
    }elseif ($log == 'pembayaran') {
        $sql = 'SELECT * FROM riwayat_pinjaman where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id_pinjaman desc limit 1';
        $query = mysqli_query($this->conn(), $sql);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            $fetch = mysqli_fetch_array($query);
            $last  = $fetch['hutang_akhir'] - $valArr[3];
            $qr = 'insert into riwayat_pinjaman (tanggal, id_anggota, nominal_in, hutang_akhir, keperluan, no_pembayaran) values ('.$val[3].', \''.trim($valArr[1]).'\', \''.$valArr[3].'\', \''.$last.'\', \''.$log.'\', \''.$arr['no_pembayaran'].'\')';            
            $query = mysqli_query($this->conn(), $qr);
        }
    }
}
public function log_transaksi($arr='', $log, $field, $val)
{
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    if($log == 'simpanan'){
        $sql = 'SELECT * FROM riwayat_transaksi where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id_transaksi desc limit 1';
        $query = mysqli_query($this->conn(), $sql);
        $rowcount = mysqli_num_rows($query);
        if(!$rowcount){
            $qr = 'insert into riwayat_transaksi (tanggal, id_anggota, nominal_in, nominal_akhir, keperluan, no_simpanan) values ('.$val[3].', \''.trim($valArr[1]).'\', \''.$valArr[3].'\', \''.$valArr[3].'\', \''.$log.'\', \''.$arr['no_simpanan'].'\')';
            $query = mysqli_query($this->conn(), $qr);
        } else{
            $fetch = mysqli_fetch_array($query);
            $last  = $fetch['nominal_akhir'] + $valArr[3];
            $qr = 'insert into riwayat_transaksi (tanggal, id_anggota, nominal_in, nominal_akhir, keperluan, no_simpanan) values ('.$val[3].', \''.trim($valArr[1]).'\', \''.$valArr[3].'\', \''.$last.'\', \''.$log.'\', \''.$arr['no_simpanan'].'\')';
            $query = mysqli_query($this->conn(), $qr);
        }
    }elseif ($log == 'pencairan') {
        $sql = 'SELECT * FROM riwayat_transaksi where '. $keyArr[1].' = \''.trim($valArr[1]).'\'  order by id_transaksi desc limit 1';
        $query = mysqli_query($this->conn(), $sql);
        $rowcount = mysqli_num_rows($query);
        if($rowcount){
            $fetch = mysqli_fetch_array($query);
            $last  = $fetch['nominal_akhir'] - $valArr[3];
            $qr = 'insert into riwayat_transaksi (tanggal, id_anggota, nominal_out, nominal_akhir, keperluan, no_pencairan) values ('.$val[3].', \''.trim($valArr[1]).'\', \''.$valArr[3].'\', \''.$last.'\', \''.$log.'\', \''.$arr['no_pencairan'].'\')';
            $query = mysqli_query($this->conn(), $qr);
        }
    }
   
}
function genUser($arr = null){    
    $keyArr = array_keys($arr);
    $valArr = array_values($arr);
    $id_generate = '';
    if($valArr[1] == 'admin'){
        $sql = "SELECT max(`id_user`) as id_user FROM users where left(id_user, 4) = 'IKGK'";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['id_user'] == null or $getId['id_user'] == ''){
                $id_user = "IKGK0001";
            }else{
                $id_user = (int) substr($getId['id_user'], 4, 6)+1;
                $char = "IKGK";
                $id_generate = $char . sprintf("%04s", $id_user);
            }
    } elseif($valArr[1] == 'anggota') {
        $sql = "SELECT max(`id_anggota`) as id_anggota FROM anggota";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['id_anggota'] == null or $getId['id_anggota'] == ''){
                $id_generate = "0001/IKGK/".date('y');
            }else{
                $id_anggota = explode('/', $getId['id_anggota']) [0] + 1;
                $char = "IKGK";
                $id_generate = sprintf("%04s", $id_anggota).'/'.$char.'/'.date('y');
            }
    } elseif($valArr[1] == 'no_simpanan') {
        $sql = "SELECT max(`no_simpanan`) as no_simpanan FROM simpanan";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['no_simpanan'] == null or $getId['no_simpanan'] == ''){
                $id_generate = date('ym').'-SP-'.'0001';
            }else{
                $no_simpanan = explode('-', $getId['no_simpanan']) [2] + 1;
                $char = "IKGK";
                $id_generate = date('ym').'-SP-'.sprintf("%04s", $no_simpanan);
            }
    } elseif($valArr[1] == 'no_pencairan') {
        $sql = "SELECT max(`no_pencairan`) as no_pencairan FROM pencairan_simpanan";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['no_pencairan'] == null or $getId['no_pencairan'] == ''){
                $id_generate = date('ym').'-CAIR-'.'0001';
            }else{
                $no_pencairan = explode('-', $getId['no_pencairan']) [2] + 1;
                $id_generate = date('ym').'-CAIR-'.sprintf("%04s", $no_pencairan);
            }
    }  elseif($valArr[1] == 'no_pinjaman') {
        $sql = "SELECT max(`no_pinjaman`) as no_pinjaman FROM pinjaman";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['no_pinjaman'] == null or $getId['no_pinjaman'] == ''){
                $id_generate = date('ym').'-PJ-'.'0001';
            }else{
                $no_pinjaman = explode('-', $getId['no_pinjaman']) [2] + 1;
                $char = "IKGK";
                $id_generate = date('ym').'-PJ-'.sprintf("%04s", $no_pinjaman);
            }
    } elseif($valArr[1] == 'no_pembayaran') {
        $sql = "SELECT max(`no_pembayaran`) as no_pembayaran FROM pembayaran_pinjaman";
        $query = mysqli_query($this->conn(), $sql);
        $getId = mysqli_fetch_array($query);
            if($getId['no_pembayaran'] == null or $getId['no_pembayaran'] == ''){
                $id_generate = date('ym').'-BAYAR-'.'0001';
            }else{
                $no_pembayaran = explode('-', $getId['no_pembayaran']) [2] + 1;
                $id_generate = date('ym').'-BAYAR-'.sprintf("%04s", $no_pembayaran);
            }
    }
    echo json_encode(array('error'=>0,'msg'=> 'Generate ID berhasil', 'json' => $id_generate));
    
}

function getKeysArr($dA='')
{
    $i          = 0;
    $key_ready  = array();
    foreach ($dA as $key => $value) {
        if($value[$i] == '' || $value[$i] == null ){
            continue;
        } else {
            $key_ready [] = $key;
        }
    }
    return $key_ready;
    
}
function session($arr){
    session_start();
    $key = $this->getKeysArr($arr);
    foreach ($key as $e) {
	if(is_string($e)){
           $_SESSION[$e]=$arr[$e];
	}
    }
}

}
