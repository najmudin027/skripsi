 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <div class="portlet-title">
        <div class="caption">
            Data Anggota Koperasi 
        <div class="pull-right">
        <small class="btn btn-primary btn-sm" onclick="addData()">
        <i class="glyphicon glyphicon-plus" ></i> Tambah Anggota Koperasi
        </small>
        </div>
        </div>
    </div>
    <hr>
    <table class="table table-striped table-condensed table-bordered user">
        <thead>
            <tr>      
                <th>ID Anggota</th>
                <th>Nama</th>
                <th>No. Telp</th>
                <th>Alamat</th>
                <th>Tanggal bergabung</th>
                <th>Keanggotaan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $class = new Class_lib();
        $sql = "SELECT * FROM anggota";
        $query = mysqli_query($class->conn(), $sql);
        while ($row = mysqli_fetch_array($query)){ ?>
            <tr>
                <td><?php echo $row['id_anggota'];?></td>
                <td><?php echo $row['nm_lengkap'];?></td>
                <td><?php echo $row['no_telp'];?></td>
                <td><?php echo $row['alamat'];?></td>
                <td><?php echo $row['tgl_bergabung'];?></td>
                <td class="text-center"><?php echo ucwords($row['keanggotaan']);?></td>
                <td>
                <center>
                 <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('anggota','<?php echo $row['id_anggota'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                </center>
                </td>
            </tr>
        <?php };?>
        </tbody>
    </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Tambah Anggota Koperasi</h4>
      </div>
      <div class="modal-body">
      <form method="post" id="add_data">
        <input type="hidden" name="add_data" value="anggota">
        <div class="form-group">
            <label for="">ID Anggota Koperasi</label>
            <input type="text" name="id_anggota" id="id_anggota" readonly="" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nm_lengkap" placeholder="Masukan nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">No Induk Pegawai</label>
            <input type="text" name="nip" placeholder="Masukan no induk pegawai" class="form-control" required>
        </div>
         <div class="form-group">
            <label for="">Jenis Kelamin</label>
            <select name="jns_kelamin" id="jns_kelamin"  class="form-control">
                <option value="Laki-Laki">Laki - Laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tempat Lahir</label>
            <input type="text" id="tmpt_lahir" name="tmpt_lahir" class="form-control" required="">
        </div>
        <div class="form-group">
            <label for="">Tanggal Lahir</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" class="form-control" id="datepicker" value="<?php echo date('d-m-Y')?>" name="tgl_lahir">
            </div>
        </div>
        <div class="form-group">
            <label for="">No. KTP</label>
            <input type="text" name="no_ktp" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">No. Telp</label>
            <input type="number" name="no_telp" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" id="alamat" cols="6" rows="3" class="form-control "></textarea>
        </div>
        <div class="form-group">
            <label for="">Jabatan</label>
            <select name="id_jabatan" id="id_jabatan" class="form-control" required="">
                <?php
                $class = new Class_lib();
                $sql = "SELECT * FROM jabatan order by nm_jabatan desc";
                $query = mysqli_query($class->conn(), $sql);
                $no = 1;
                while ($row = mysqli_fetch_array($query)){ ?>
                <option value="<?php echo $row['id_jabatan'] ?>"><?php echo $row['nm_jabatan'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Status</label>
            <select name="id_status" id="id_status" class="form-control" required="">
                <?php
                $class = new Class_lib();
                $sql = "SELECT * FROM status order by nm_status desc";
                $query = mysqli_query($class->conn(), $sql);
                $no = 1;
                while ($row = mysqli_fetch_array($query)){ ?>
                <option value="<?php echo $row['id_status'] ?>"><?php echo $row['nm_status'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tanggal Bergabung</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" class="form-control" id="datepicker1" value="<?php echo date('d-m-Y')?>" name="tgl_bergabung">
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Tambah"> <br>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Edit Admin Koperasi</h4>
    </div>
      <div class="modal-body">
      <form method="post" id="edit_data">
        <input type="hidden" name="edit_data" value="anggota">
        <input type="hidden" name="primary" value="id_anggota">
        <input type="hidden" name="id_data" id="id_data">
        <div class="form-group">
            <label for="">ID Anggota Koperasi</label>
            <input type="text" name="id_anggota" id="id_anggota1" readonly="" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nm_lengkap" id="nm_lengkap1" placeholder="Masukan nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Jenis Kelamin</label>
            <select name="jns_kelamin" id="jns_kelamin1"  class="form-control">
                <option value="Laki-Laki">Laki - Laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tempat Lahir</label>
            <input type="text"  name="tmpt_lahir" id="tmpt_lahir1" class="form-control" required="">
        </div>
        <div class="form-group">
            <label for="">Tanggal Lahir</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" class="form-control" id="datepicker2" value="<?php echo date('d-m-Y')?>" name="tgl_lahir">
            </div>
        </div>
        <div class="form-group">
            <label for="">No. KTP</label>
            <input type="text" name="no_ktp" id="no_ktp1" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">No. Telp</label>
            <input type="text" name="no_telp" id="no_telp1" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" id="alamat1" cols="6" rows="3" class="form-control "></textarea>
        </div>
        <div class="form-group">
            <label for="">Jabatan</label>
            <select name="id_jabatan" id="id_jabatan1" class="form-control" required="">
                <?php
                $class = new Class_lib();
                $sql = "SELECT * FROM jabatan order by nm_jabatan desc";
                $query = mysqli_query($class->conn(), $sql);
                $no = 1;
                while ($row = mysqli_fetch_array($query)){ ?>
                <option value="<?php echo $row['id_jabatan'] ?>"><?php echo $row['nm_jabatan'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Status</label>
            <select name="id_status" id="id_status1" class="form-control" required="">
                <?php
                $class = new Class_lib();
                $sql = "SELECT * FROM status order by nm_status desc";
                $query = mysqli_query($class->conn(), $sql);
                $no = 1;
                while ($row = mysqli_fetch_array($query)){ ?>
                <option value="<?php echo $row['id_status'] ?>"><?php echo $row['nm_status'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tanggal Bergabung</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" class="form-control" id="datepicker3" value="<?php echo date('d-m-Y')?>" name="tgl_bergabung">
            </div>
        </div>
        <div class="form-group">
            <label for="">Keanggotaan</label>
            <select name="keanggotaan" id="keanggotaan1"  class="form-control">
                <option value="aktif">Aktif</option>
                <option value="tidak aktif">Tidak Aktif</option>
            </select>
        </div>
        <input type="submit" class="btn btn-primary" value="Perbaharui"> <br>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal edit data end--}} -->
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
 <script>
function addData(){
    $('#myModal').modal('show');
}
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            $('#editModal input#id_data').val(data.json.id_anggota)
            $('#editModal input#id_anggota1').val(data.json.id_anggota)
            $('#editModal input#nm_lengkap1').val(data.json.nm_lengkap)
            $('#editModal input#no_telp1').val(data.json.no_telp)
            $('#editModal textarea#alamat1').val(data.json.alamat)
            $('#editModal input#tmpt_lahir1').val(data.json.tmpt_lahir)
            $('#editModal input#no_ktp1').val(data.json.no_ktp)
            $('#editModal input#no_telp1').val(data.json.no_telp)
        }
    }
    });
}
$(document).ready(function(){
//  set datepicker
$("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
$("#datepicker1").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
//  set datepicker
$("#datepicker2").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
$("#datepicker3").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'anggota', id_anggota : 'anggota', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#id_anggota').val(data.json);
            }else{
                PNotify.error({
                    title: 'Error',
                    text: 'Error. ' + data.msg,
                    type: 'error'
                });
            }

        },error:function(){
            PNotify.error({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
        }
    });
});
// stop the form from submitting the normal way and refreshing the page
// process the form
$('form#add_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#add_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
// process edit form
$('form#edit_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#edit_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
 </script>