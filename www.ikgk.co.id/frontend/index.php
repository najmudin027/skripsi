 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Anda berada di halaman utama <span class="kepala">admin</span>.
    </div>
    <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12 text-center" id="left">
        <img  width="180"
            src="<?php echo $base_url ?>assets/images/koperasi.jpg">
    </div>
    <div class="col-md-9 col-sm-6 col-xs-12">
    <table>
      <tr><td><b class="text-info">Nama User</b></td></tr>
      <tr><td><p class="text-danger"><?php echo ucwords($_SESSION['nm_user'])?></p></td></tr>
      <tr><td><b class="text-info">Halaman</b></td></tr>
      <tr><td><p class="text-danger"><span class="kepala">Administrasi</span></p></td></tr>
      <tr><td><b class="text-info">Tanggal</b></td></tr>
      <tr><td><p class="text-danger"><?=date('m / d / Y'); ?></p></td></tr>
      <tr><td><b class="text-info">Browser</b></td></tr>
      <tr><td><p class="text-danger"><?php echo $_SERVER['HTTP_USER_AGENT'] ?></p></td></tr>
      <tr><td><b class="text-info">IP Komputer</b></td></tr>
      <tr><td><p class="text-danger"><?php echo $_SERVER['REMOTE_ADDR'];; ?></p></td></tr>
      <tr><td><b class="text-info">Nama Komputer</b></td></tr>
      <tr><td><p class="text-danger"><?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']);; ?></p></td></tr>
    </table>
    </div>
    </div>    
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>