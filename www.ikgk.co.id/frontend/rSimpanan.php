 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Simpanan">Data Riwayat</a></li>
        <li><a data-toggle="tab" href="#cair_anggota">Riwayat Riwayat Simpanan</a></li>
    </ul>

    <div class="tab-content">
        <div id="Simpanan" class="tab-pane fade in active">
          <br>
        <table class="table table-striped table-condensed table-bordered user">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>ID Anggota</th>
                    <th>Nominal Masuk</th>
                    <th>Nominal Keluar</th>
                    <th>Nominal Akhir</th>
                    <th>Keperluan</th>
                    <th>No Simpanan</th>
                    <th>No Pencairan</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $class = new Class_lib();
            $sql = "SELECT * FROM riwayat_transaksi";
            $query = mysqli_query($class->conn(), $sql);
            $no = 1;
            while ($row = mysqli_fetch_array($query)){ ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row['tanggal'];?></td>
                    <td><?php echo $row['id_anggota'];?></td>
                    <td><?php echo 'Rp '.number_format($row['nominal_in']);?></td>
                    <td><?php echo 'Rp '.number_format($row['nominal_out']);?></td>
                    <td><?php echo 'Rp '.number_format($row['nominal_akhir']);?></td>
                    <td><?php echo $row['keperluan'];?></td>
                    <td><?php echo $row['no_simpanan'];?></td>
                    <td><?php echo $row['no_pencairan'];?></td>
                </tr>
            <?php $no++; };?>
            </tbody>
        </table>
        </div>
        <div id="cair_anggota" class="tab-pane fade">
          <br>
          <div class="form-group" id="cariCair">
          <label for="" class="col-sm-4 control-label">Masukkan No. Anggota</label>
          <div class="col-sm-8">
              <div class="input-group">
              <input type="text" class="form-control" name="id_cair" id="id_cair">
              <div class="input-group-addon" onclick="editData('riwayat_transaksi','', '<?php echo $base_url ?>')"><i class="fa fa-search"></i></div>
              </div>
          </div>
        </div>
        <br>  
        <div class="row" id="resultSearch">
          <div class="col-md-12">
          <hr>
          <div class="portlet-title">
              <div class="caption">
                  Data Riwayat Simpanan 
              <div class="pull-right">
              <small class="btn btn-primary btn-sm" onclick="ctkData('<?php echo $base_url ?>')">
              <i class="glyphicon glyphicon-print" ></i> Cetak Riwayat Simpanan
              </small>
              </div>
              </div>
          </div>
          <br>
          <table class="table table-striped table-condensed table-bordered spec_riwayat">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>ID Anggota</th>
                    <th>Nominal Masuk</th>
                    <th>Nominal Keluar</th>
                    <th>Nominal Akhir</th>
                    <th>Keperluan</th>
                    <th>No Simpanan</th>
                    <th>No Pencairan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
        </div>
        </div>
    </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
<script>
$(document).ready(function(){
  $("#formAdd").hide();
  $("#resultSearch").hide();
  });
function addData(){
    $('#myModal').modal('show');
}
function ctkData(base) {
  var id_riwayat = $('#cariCair input#id_cair').val();
  url = base +'frontend/cetak/cetak_transaksi.php?id_anggota=' +id_riwayat;
  window.open(url);
}
function editData(table, id, base){
    var id_cair = $('#cariCair input#id_cair').val();
    $('#resultSearch input#id_cetak').val(id_cair);
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id_anggota: id_cair, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.error == '0') {
            $("#resultSearch").show("slow");
            $('table.spec_riwayat tbody >tr').remove();
            no = 1;
            $.each(data.json, function(key, value){
                $('table.spec_riwayat').append(
                '<tr>'+
                '<td>'+no+'</td>'+
                '<td>'+value[1]+'</td>'+
                '<td class="text-center">'+value[2]+'</td>'+
                '<td>Rp '+idr(value[3])+'</td>'+
                '<td>Rp '+idr(value[4])+'</td>'+
                '<td>Rp '+idr(value[5])+'</td>'+
                '<td class="text-center">'+value[6]+'</td>'+
                '<td class="text-center">'+value[7]+'</td>'+
                '<td class="text-center">'+value[8]+'</td>'+
                '</tr>'
                );
                no++;
            });
        } else{
          $("#resultSearch").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
// int to idr format
function idr(e) {
  var rp = parseInt(e);
    return new Intl.NumberFormat('ID').format(rp);
}
function search(base, table){
    var id_search = $('#cariData input#id_search').val();
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id_search, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $("#formAdd").show("slow");
            $("#formAdd #id_anggota").val(id_search);
            getLastNominal(base, id_search);
            getNoPencairan();

        } else{
            $("#formAdd").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
function getLastNominal(base, id_search){
   nominal = 0;
   $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: 'riwayat_transaksi', id_anggota: id_search, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            nominal = 0;
            nominal = data.json.nominal_akhir;
             $('#sisa_simpanan').text(idr(nominal));
             $('#get_last').val(nominal)
        }
    }
    });
}
function getNoPencairan() {
  // process the request
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
      data        :{table: 'pencairan', no_pencairan : 'no_pencairan', method: 'generate'}, // our data object
      success: function (data) {
              data = JSON.parse(data);
              if(data.error == 0) {
                  $('#formAdd #no_pencairan').val(data.json);
              }else{
                  PNotify.error({
                      title: 'Error',
                      text: 'Error. ' + data.msg,
                      type: 'error'
                  });
              }

          },error:function(){
              PNotify.error({
                  title: 'Error',
                  text: 'Error. Please check your connection',
                  type: 'error'
              });
          }
      });
  // stop the form from submitting the normal way and refreshing the page
}
// process the form
$('form#add_data').submit(function(event) {
var res =  $('#get_last').val()
var nominal = $('#add_data #nominal').val();
// console.log(res+' '+nominal)
if(parseInt(nominal) > parseInt(res)){
  PNotify.error({
        title: 'Error',
        text: 'Error. Nominal yang anda masukkan melebihi dari jumlah simpanan anda.',
        type: 'error'
    });
} else{
// process the form
$.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
      data        : $('form#add_data').serialize(), // our data object
      success: function (data) {
              data = JSON.parse(data);
              if(data.error == 0) {
                  PNotify.success({
                    title: 'Success!',
                    text: data.msg
                  });
                  setTimeout(function(){ location.reload(); }, 1000);
              }else{
                  PNotify.error({
                      title: 'Error',
                      text: 'Error. ' + data.msg,
                      type: 'error'
                  });
              }

          },error:function(){
              PNotify.error({
                  title: 'Error',
                  text: 'Error. Please check your connection',
                  type: 'error'
              });
          }
      });
  // stop the form from submitting the normal way and refreshing the page
}
  event.preventDefault();
});
</script>