 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <div class="portlet-title">
        <div class="caption">
            Data Jabatan Anggota 
        <div class="pull-right">
        <small class="btn btn-primary btn-sm" onclick="addData()">
        <i class="glyphicon glyphicon-plus" ></i> Tambah Jabatan Anggota
        </small>
        </div>
        </div>
    </div>
    <hr>
    <table class="table table-striped table-condensed table-bordered user">
        <thead>
            <tr>            
                <th>#</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $class = new Class_lib();
        $sql = "SELECT * FROM jabatan";
        $query = mysqli_query($class->conn(), $sql);
        $no = 1;
        while ($row = mysqli_fetch_array($query)){ ?>
            <tr>
                <td><?php echo $no;?></td>
                <td><?php echo $row['nm_jabatan'];?></td>
                <td>
                <center>
                 <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('jabatan','<?php echo $row['id_jabatan'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                 <a class="btn btn-danger btn-sm btnEdit" href="javascript:void(0)"  onclick="destroyData('jabatan','<?php echo $row['id_jabatan'];?>', '<?php echo $base_url ?>config/process.php')"><i class="fa fa-trash"></i></a>
                </center>
                </td>
            </tr>
        <?php $no++; };?>
        </tbody>
    </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Tambah data jabatan</h4>
      </div>
      <div class="modal-body">
      <form method="post" id="add_data">
        <input type="hidden" name="add_data" value="jabatan">
        <div class="form-group">
            <label for="">Nama jabatan</label>
            <input type="text" name="nm_jabatan" placeholder="Masukan nama jabatan" class="form-control" required>
        </div>
        <input type="submit" class="btn btn-primary" value="Kirim"> <br>
            
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Edit data jabatan</h4>
    </div>
    <div class="modal-body">
    <form method="post" id="edit_data">
    <input type="hidden" name="edit_data" value="jabatan">
    <input type="hidden" name="primary" value="id_jabatan">
    <input type="hidden" name="id_data" id="id_data">
    <div class="form-group">
        <label for="">Nama jabatan</label>
        <input type="text" name="nm_jabatan" id="nm_jabatan" placeholder="Masukan nama jabatan" class="form-control" required>
    </div>
    <input type="submit" class="btn btn-primary" value="Kirim"> <br>
        
    </form>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<!-- {{-- modal edit data end--}} -->
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
 <script>
function addData(){
    $('#myModal').modal('show');
}
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            $('#edit_data input#id_data').val(data.json.id_jabatan)
            $('#edit_data input#nm_jabatan').val(data.json.nm_jabatan)
        }
    }
    });
}
$(document).ready(function(){
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'users', id_user : 'admin', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#id_user').val(data.json);
            }else{
                PNotify.error({
                    title: 'Error',
                    text: 'Error. ' + data.msg,
                    type: 'error'
                });
            }

        },error:function(){
            PNotify.error({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
        }
    });
});
// stop the form from submitting the normal way and refreshing the page
// process the form
$('form#add_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#add_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
// process edit form
$('form#edit_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#edit_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
 </script>