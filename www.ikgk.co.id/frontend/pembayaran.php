 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Pinjaman">Data Pembayaran</a></li>
        <li><a data-toggle="tab" href="#cair_anggota">Riwayat Pembayaran Pinjaman</a></li>
    </ul>

    <div class="tab-content">
        <div id="Pinjaman" class="tab-pane fade in active">
          <br>
          <div class="portlet-title">
            <div class="caption">
                Data Pembayaran Pinjaman 
            <div class="pull-right">
            <small class="btn btn-primary btn-sm" onclick="addData()">
            <i class="glyphicon glyphicon-plus" ></i> Pembayaran  Pinjaman
            </small>
            </div>
            </div>
        </div>
        <hr>
        <table class="table table-striped table-condensed table-bordered user">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>No Pembayaran</th>
                    <th>ID Anggota</th>
                    <th>Nominal</th>
                    <th>Tanggal Setor</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $class = new Class_lib();
            $sql = "SELECT * FROM pembayaran_pinjaman";
            $query = mysqli_query($class->conn(), $sql);
            $no = 1;
            while ($row = mysqli_fetch_array($query)){ ?>
                <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $row['no_pembayaran'];?></td>
                    <td><?php echo $row['id_anggota'];?></td>
                    <td><?php echo 'Rp '.number_format($row['nominal']);?></td>
                    <td><?php echo $row['tgl_bayar'];?></td>
                </tr>
            <?php $no++; };?>
            </tbody>
        </table>
        </div>
        <div id="cair_anggota" class="tab-pane fade">
          <br>
          <div class="form-group" id="cariBayar">
          <label for="" class="col-sm-4 control-label">Masukkan No. Anggota</label>
          <div class="col-sm-8">
              <div class="input-group">
              <input type="text" class="form-control" name="id_bayar" id="id_bayar">
              <div class="input-group-addon" onclick="editData('pembayaran_pinjaman','', '<?php echo $base_url ?>')"><i class="fa fa-search"></i></div>
              </div>
          </div>
        </div>
        <br>  
        <div class="row" id="resultSearch">
          <div class="col-md-12">
          <hr>
          <table class="table table-striped table-condensed table-bordered spec_pembayaran">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>No Pembayaran</th>
                    <th>ID Anggota</th>
                    <th>Nominal</th>
                    <th>Tanggal Pembayaran</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
        </div>
        </div>
    </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Pembayaran Pinjaman Anggota</h4>
      </div>
      <div class="modal-body">
      <div class="form-group" id="cariData">
          <label for="" class="col-sm-4 control-label">Masukkan No. Anggota</label>
          <div class="col-sm-8">
              <div class="input-group">
              <input type="text" class="form-control" name="id_search" id="id_search">
              <div class="input-group-addon" onclick="search('<?php echo $base_url;?>', 'anggota')"><i class="fa fa-search"></i></div>
              </div>
          </div>
      </div>
      <br>  
      <div class="row" id="formAdd">
        <div class="col-md-12">
          <br>
          <label for="">Jumlah Pinjaman Anda tersisa : Rp. <span id="sisa_pinjaman"></span></label>
          <input type="hidden" id="get_last">
        <hr>
        <form action="" id="add_data">
          <input type="hidden" name="add_data" value="pembayaran_pinjaman">
          <input type="hidden" name="id_anggota" id="id_anggota">
          <div class="form-group">
              <label for="">Nomor pencairan</label>
              <input type="text" name="no_pembayaran" id="no_pembayaran" class="form-control" readonly="">
          </div>
          <div class="form-group">
              <label for="">Nominal</label>
              <input type="number" name="nominal" id="nominal" placeholder="Masukan nominal" class="form-control" required>
          </div>
          <div class="form-group">
              <label for="">Tanggal Pembayaran</label>
              <input type="text" class="form-control" id="datepicker" value="<?php echo date('d-m-Y')?>" name="tgl_bayar">
          </div>
          <input type="submit" class="btn btn-primary" value="Kirim"> <br>
        </form>
      </div>
      </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
<script>
$(document).ready(function(){
  $("#formAdd").hide();
  $("#resultSearch").hide();
  });
function addData(){
    $('#myModal').modal('show');
}
function editData(table, id, base){
    var id_bayar = $('#cariBayar input#id_bayar').val();
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id_anggota: id_bayar, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.error == '0') {
            $("#resultSearch").show("slow");
            $('table.spec_pembayaran tbody >tr').remove();
            no = 1;
            $.each(data.json, function(key, value){
                $('table.spec_pembayaran').append(
                '<tr>'+
                '<td>'+no+'</td>'+
                '<td>'+value[0]+'</td>'+
                '<td class="text-center">'+value[1]+'</td>'+
                '<td>Rp '+idr(value[2])+'</td>'+
                '<td class="text-center">'+value[3]+'</td>'+
                '</tr>'
                );
                no++;
            });
        } else{
          $("#resultSearch").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
// int to idr format
function idr(e) {
  var rp = parseInt(e);
    return new Intl.NumberFormat('ID').format(rp);
}
function search(base, table){
    var id_search = $('#cariData input#id_search').val();
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id_search, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $("#formAdd").show("slow");
            $("#formAdd #id_anggota").val(id_search);
            getLastNominal(base, id_search);
            getNoPembayaran();

        } else{
            $("#formAdd").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
function getLastNominal(base, id_search){
    var nominal = 0;
   $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: 'riwayat_pinjaman', id_anggota: id_search, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
          var nominal = 0;
              nominal = data.json[0][5];
             $('#sisa_pinjaman').text(idr(nominal));
             $('#get_last').val(nominal)
        }
    }
    });
}
function getNoPembayaran() {
  // process the request
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
      data        :{table: 'pembayaran_pinjaman', no_pembayaran : 'no_pembayaran', method: 'generate'}, // our data object
      success: function (data) {
              data = JSON.parse(data);
              if(data.error == 0) {
                  $('#formAdd #no_pembayaran').val(data.json);
              }else{
                  PNotify.error({
                      title: 'Error',
                      text: 'Error. ' + data.msg,
                      type: 'error'
                  });
              }

          },error:function(){
              PNotify.error({
                  title: 'Error',
                  text: 'Error. Please check your connection',
                  type: 'error'
              });
          }
      });
  // stop the form from submitting the normal way and refreshing the page
}
// process the form
$('form#add_data').submit(function(event) {
var res =  $('#get_last').val()
var nominal = $('#add_data #nominal').val();
// console.log(res+' '+nominal)
if(parseInt(nominal) > parseInt(res)){
  PNotify.error({
        title: 'Error',
        text: 'Error. Nominal yang anda masukkan melebihi dari jumlah simpanan anda.',
        type: 'error'
    });
} else if(res == ""){
   PNotify.error({
        title: 'Error',
        text: 'Error. Anda tidak memiliki pinjaman untuk dibayar.',
        type: 'error'
    });
}else{
// process the form
$.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
      data        : $('form#add_data').serialize(), // our data object
      success: function (data) {
              data = JSON.parse(data);
              if(data.error == 0) {
                  PNotify.success({
                    title: 'Success!',
                    text: data.msg
                  });
                  setTimeout(function(){ location.reload(); }, 1000);
              }else{
                  PNotify.error({
                      title: 'Error',
                      text: 'Error. ' + data.msg,
                      type: 'error'
                  });
              }

          },error:function(){
              PNotify.error({
                  title: 'Error',
                  text: 'Error. Please check your connection',
                  type: 'error'
              });
          }
      });
  // stop the form from submitting the normal way and refreshing the page
}
  event.preventDefault();
});
</script>