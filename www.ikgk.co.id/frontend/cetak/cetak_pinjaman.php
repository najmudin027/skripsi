<?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	// include_once('../../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../../config/Class_lib.php');
	// memanggil library FPDF
	require('fpdf.php');
	// intance object dan memberikan pengaturan halaman PDF
	$pdf = new FPDF('P','mm','A4');
	// membuat halaman baru
	$pdf->AddPage();
	// setting jenis font yang akan digunakan
	$pdf->SetFont('Arial','B',16);
	// mencetak string 
	$pdf->Cell(190,7,'KOPERASI PEGAWAI SEKOLAH DASAR NEGERI KRUKUT 01',0,1,'C');
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(190,7,'Alamat: Jl. KH Zainul Arifin No. 4, RT.5/RW.7, Krukut, Kec. Taman Sari,  ',0,1,'C');
	$pdf->Cell(190,7,'Kota Jakarta Barat,Daerah Khusus Ibukota Jakarta 11140.',0,1,'C');
	$pdf->Cell(190,2,'(Laporan Riwayat Pinjaman)',0,1,'C');

	// Memberikan space kebawah agar tidak terlalu rapat
	$pdf->Cell(10,7,'',0,1);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(20,6,'Tanggal',1,0);
	$pdf->Cell(35,6,'ID Anggota',1,0);
	$pdf->Cell(35,6,'Nominal Masuk',1,0);
	$pdf->Cell(35,6,'Nominal Keluar',1,0);
	$pdf->Cell(35,6,'Hutang Akhir',1,0);
	$pdf->Cell(32,6,'Keperluan',1,1);
	// $pdf->Cell(27,6,'Nomor Simpanan',1,0);
	// $pdf->Cell(27,6,'Nomor Pencairan',1,1);

	$pdf->SetFont('Arial','',10);
	$id_anggota = $_GET['id_anggota'];
	$conn = mysqli_connect($host = 'localhost', $username = 'root', $password ='', $database = 'db_ikgk');
	$qry = 'SELECT * FROM riwayat_pinjaman where id_anggota = \''.trim($id_anggota).'\' ';
    $sql = mysqli_query($conn, $qry);  
	while ($row = mysqli_fetch_array($sql)){
	    $pdf->Cell(20,6,$row['tanggal'],1,0);
	    $pdf->Cell(35,6,$row['id_anggota'],1,0); 
	    $pdf->Cell(35,6,'Rp '.number_format($row['nominal_in']),1,0);
	    $pdf->Cell(35,6,'Rp '.number_format($row['nominal_out']),1,0);
	    $pdf->Cell(35,6,'Rp '.number_format($row['hutang_akhir']),1,0); 
	    $pdf->Cell(32,6,$row['keperluan'],1,1); 
	}

$pdf->Output();

 }
 ?>