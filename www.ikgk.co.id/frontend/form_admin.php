 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <div class="portlet-title">
        <div class="caption">
            Data Admin Koperasi 
        <div class="pull-right">
        <small class="btn btn-primary btn-sm" onclick="addData()">
        <i class="glyphicon glyphicon-plus" ></i> Tambah Admin Koperasi
        </small>
        </div>
        </div>
    </div>
    <hr>
    <table class="table table-striped table-condensed table-bordered user">
        <thead>
            <tr>      
                <th>ID Admin</th>
                <th>Nama</th>
                <th>No. Telp</th>
                <th>Alamat</th>
                <th>Username</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $class = new Class_lib();
        $sql = "SELECT * FROM users";
        $query = mysqli_query($class->conn(), $sql);
        while ($row = mysqli_fetch_array($query)){ ?>
            <tr>
                <td><?php echo $row['id_user'];?></td>
                <td><?php echo $row['nm_user'];?></td>
                <td><?php echo $row['no_telp'];?></td>
                <td><?php echo $row['alamat'];?></td>
                <td><?php echo $row['username'];?></td>
                <td>
                <center>
                 <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="editData('users','<?php echo $row['id_user'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                 <a class="btn btn-danger btn-sm btnEdit" href="javascript:void(0)"  onclick="destroyData('users', '<?php echo $row['id_user'];?>', '<?php echo $base_url ?>')"><i class="fa fa-trash"></i></a>
                </center>
                </td>
            </tr>
        <?php };?>
        </tbody>
    </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Tambah Admin Koperasi</h4>
      </div>
      <div class="modal-body">
      <form method="post" id="add_data">
        <input type="hidden" name="add_data" value="users">
        <div class="form-group">
            <label for="">ID Admin Koperasi</label>
            <input type="text" name="id_user" id="id_user" readonly="" class="form-control">
         </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nm_user" placeholder="Masukan nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">No. Telp</label>
            <input type="number" name="no_telp" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" id="alamat" cols="6" rows="3" class="form-control "></textarea>
        </div>
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" name="username" minlength="6" placeholder="Masukan username" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" minlength="6" placeholder="Masukan password" class="form-control" required>
        </div>
        <input type="submit" class="btn btn-primary" value="Tambah"> <br>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Edit Admin Koperasi</h4>
    </div>
      <div class="modal-body">
      <form method="post" id="edit_data">
        <input type="hidden" name="edit_data" value="users">
        <input type="hidden" name="primary" value="id_user">
        <input type="hidden" name="id_data" id="id_data">
        <div class="form-group">
            <label for="">ID Admin Koperasi</label>
            <input type="text" name="id_user" id="id_user1" readonly="" class="form-control">
         </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nm_user" id="nm_user1" placeholder="Masukan nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">No. Telp</label>
            <input type="text" name="no_telp" id="no_telp1" placeholder="Masukan no. telp" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" id="alamat1" cols="6" rows="3" class="form-control "></textarea>
        </div>
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" name="username" id="username1" minlength="6" placeholder="Masukan username" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" id="password" minlength="6" placeholder="Masukan password" class="form-control">
        </div>
        <input type="submit" class="btn btn-primary" value="Perbaharui"> <br>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal edit data end--}} -->
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
 <script>
function addData(){
    $('#myModal').modal('show');
}
function editData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            $('#editModal input#id_data').val(data.json.id_user)
            $('#editModal input#id_user1').val(data.json.id_user)
            $('#editModal input#nm_user1').val(data.json.nm_user)
            $('#editModal input#no_telp1').val(data.json.no_telp)
            $('#editModal textarea#alamat1').val(data.json.alamat)
            $('#editModal input#username1').val(data.json.username)
        }
    }
    });
}
$(document).ready(function(){
// process the request
$.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
    data        :{table: 'users', id_user : 'admin', method: 'generate'}, // our data object
    success: function (data) {
            data = JSON.parse(data);
            if(data.error == 0) {
                $('#id_user').val(data.json);
            }else{
                PNotify.error({
                    title: 'Error',
                    text: 'Error. ' + data.msg,
                    type: 'error'
                });
            }

        },error:function(){
            PNotify.error({
                title: 'Error',
                text: 'Error. Please check your connection',
                type: 'error'
            });
        }
    });
});
// stop the form from submitting the normal way and refreshing the page
// process the form
$('form#add_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#add_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
// process edit form
$('form#edit_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#edit_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
 </script>