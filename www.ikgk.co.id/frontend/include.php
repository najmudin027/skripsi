<style>
	#content{
      margin-top: 2%;
      background: #fff;
    }
    #top1{
    	margin-bottom: 30px;
    }
    img{
      width: 150px;
      height: 150px;
    }
    h3{
	    font-weight: 600;
    }
    h3 small{
    	padding-top: 100px;
    }
    #datepicker{z-index:9999 !important}
    #datepicker1{z-index:9999 !important}
    #datepicker2{z-index:9999 !important}
    #datepicker3{z-index:9999 !important}
</style>
<body>
<div class="container" id="content">
	<div class="row">
		<div class="col-md-12" id="top">
			<div class="panel panel-default">
			  <div class="panel-body">
			    <div class="col-md-2 col-sm-6 col-xs-4">
			    	 <img src="<?php echo $base_url ?>assets/images/koperasi.jpg" alt="Logo Administrator" class="img-circle">
			    </div>
			    <div class="col-md-10 col-sm-6 col-xs-8">
			    	<div class="text-center">
			    		<h3>
			    			KOPERASI PEGAWAI SEKOLAH DASAR NEGERI KRUKUT 01 
			    		</h3>
			    		<p>
			    			Alamat: Jl. KH Zainul Arifin No. 4, RT.5/RW.7, Krukut, Kec. Taman Sari, Kota Jakarta Barat, <br> Daerah Khusus Ibukota Jakarta 11140
			    		</p>
			    	</div>
			    </div>

			  </div>
			</div>
		</div>
		<div class="col-md-12" id="top1">
			<div class="panel panel-default">
			  <div class="panel-body">
			   <ul class="nav nav-pills">
				  <li role="presentation" class="active"><a href="index.php">Home</a></li>
				  <li role="presentation"><a href="form_admin.php">Admin Koperasi</a></li>
				  <li role="presentation"><a href="anggota.php">Anggota</a></li>
				  <li role="presentation" class="dropdown">
				    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				      Master Data <span class="caret"></span>
				    </a>
				    <ul class="dropdown-menu">
				      <li role="presentation" class="active"><a href="jabatan.php">Jabatan</a></li>
					  <li role="presentation"><a href="status.php">Status</a></li>
				    </ul>
				  </li>
				  <li role="presentation" class="dropdown">
				    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				      Transaksi Koperasi <span class="caret"></span>
				    </a>
				    <ul class="dropdown-menu">
				      <li role="presentation" class="active"><a href="simpanan.php">Simpanan</a></li>
					  <li role="presentation"><a href="pinjaman.php">Pinjaman</a></li>
					  <li role="presentation"><a href="pencairan.php">Pencairan</a></li>
					  <li role="presentation"><a href="pembayaran.php">Pembayaran</a></li>
					  <li role="presentation"><a href="rSimpanan.php">Riwayat Simpanan</a></li>
					  <li role="presentation"><a href="rPinjaman.php">Riwayat Pinjaman</a></li>
				    </ul>
				  </li>
				  <li role="presentation"><a href="../logout.php">Keluar</a></li>
				</ul>
				<br>
				<div class="panel panel-default">
				  <div class="panel-body">
				  