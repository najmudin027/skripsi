 <?php
session_start();
if(isset($_SESSION['id_user']) != true){
	header("Location: ../index.php");
} else{ 
	include_once('../layout/header.php'); // Menyisipkan file header dari folder layout
    include_once('../config/Class_lib.php');
    include_once('include.php');
?>
 <div class="col-md-12" id="content-page">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Pinjaman">Data Pinjaman</a></li>
        <li><a data-toggle="tab" href="#simp_anggota">Pinjaman Anggota</a></li>
    </ul>

    <div class="tab-content">
        <div id="Pinjaman" class="tab-pane fade in active">
          <br>
          <div class="portlet-title">
            <div class="caption">
                Data Pinjaman Anggota 
            <div class="pull-right">
            <small class="btn btn-primary btn-sm" onclick="addData()">
            <i class="glyphicon glyphicon-plus" ></i> Tambah Pinjaman Anggota
            </small>
            </div>
            </div>
        </div>
        <hr>
        <table class="table table-striped table-condensed table-bordered user">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>No Pinjaman</th>
                    <th>ID Anggota</th>
                    <th>Nominal</th>
                    <th>Tanggal Pinjam</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $class = new Class_lib();
            $sql = "SELECT * FROM pinjaman";
            $query = mysqli_query($class->conn(), $sql);
            $no = 1;
            while ($row = mysqli_fetch_array($query)){ ?>
                <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $row['no_pinjaman'];?></td>
                    <td><?php echo $row['id_anggota'];?></td>
                    <td><?php echo 'Rp '.number_format($row['nominal']);?></td>
                    <td><?php echo $row['tgl_pinjaman'];?></td>
                    <td><?php echo $row['status'];?></td>
                    <td>
                    <center>
                     <a class="btn btn-primary btn-sm btnEdit" href="javascript:void(0)"  onclick="getData('pinjaman','<?php echo $row['no_pinjaman'];?>', '<?php echo $base_url ?>')"><i class="fa fa-edit"></i></a>
                    </center>
                    </td>
                </tr>
            <?php $no++; };?>
            </tbody>
        </table>
        </div>
        <div id="simp_anggota" class="tab-pane fade">
          <br>
          <div class="form-group" id="cariSimp">
          <label for="" class="col-sm-4 control-label">Masukkan No. Anggota</label>
          <div class="col-sm-8">
              <div class="input-group">
              <input type="text" class="form-control" name="id_sim" id="id_sim">
              <div class="input-group-addon" onclick="editData('pinjaman','', '<?php echo $base_url ?>')"><i class="fa fa-search"></i></div>
              </div>
          </div>
        </div>
        <br>  
        <div class="row" id="resultSearch">
          <div class="col-md-12">
          <hr>
          <table class="table table-striped table-condensed table-bordered spec_pinjaman">
            <thead>
                <tr>            
                    <th>#</th>
                    <th>No Pinjaman</th>
                    <th>ID Anggota</th>
                    <th>Nominal</th>
                    <th>Tanggal Pinjam</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
        </div>
        </div>
    </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- {{-- modal add data start--}} -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"></h4>Tambah data pinjaman</h4>
      </div>
      <div class="modal-body">
      <div class="form-group" id="cariData">
          <label for="" class="col-sm-4 control-label">Masukkan No. Anggota</label>
          <div class="col-sm-8">
              <div class="input-group">
              <input type="text" class="form-control" name="id_search" id="id_search">
              <div class="input-group-addon" onclick="search('<?php echo $base_url;?>', 'anggota')"><i class="fa fa-search"></i></div>
              </div>
          </div>
      </div>
      <br>  
      <div class="row" id="formAdd">
      <div class="col-md-12">
        <hr>
        <form action="" id="add_data">
          <input type="hidden" name="add_data" value="pinjaman">
          <input type="hidden" name="id_anggota" id="id_anggota">
          <div class="col-md-5 no-padding">
            <div class="form-group">
                <label class="control-label">Nomor Pinjaman</label><p></p>
                <input type="text" name="no_pinjaman" id="no_pinjaman" class="form-control" readonly="">
            </div>
            <div class="form-group">
              <label for="">Nominal simpanan</label>
              <input type="number" name="nominal" placeholder="Masukan nominal" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="control-label">Tanggal diajukan</label><p></p>
                <input type="text" class="form-control" name="tgl_pinjaman" id="datepicker" value="<?php echo date('d-m-Y')?>" />
            </div>
          </div>
          <div class="col-md-7">
                Rules melakukan pinjaman : <br>
                  1. Harus sudah menjadi anggota koperasi. <br>
                  2. Hanya pegawai yang sudah berstatus pegawai negeri sipil (PNS). <br>
                  3. Tidak ada tanggungan pembayaran pinjaman yang masih dibebankan. <br>
          </div>
          <div class="col-md-12 no-padding">
          <div class="form-group">
            <label for="">Alasan</label><p></p>
            <textarea name="keperluan" id="keperluan" cols="30" rows="10" class="form-control" required></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-default" type="reset">Batal</button>
            <button class="btn btn-primary" type="submit">Ajukan</button>
          </div>
          </div>
        </form>
      </div>
      </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </div>
   </div>
  </div>
<!-- {{-- modal add data end--}} -->
<!-- {{-- modal edit data start--}} -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"></h4>Edit status pinjaman</h4>
    </div>
    <div class="modal-body">
    <form method="post" id="edit_data">
    <input type="hidden" name="edit_data" value="pinjaman">
    <input type="hidden" name="primary" value="no_pinjaman">
    <input type="hidden" name="id_data" id="id_data">
    <div class="form-group">
        <label for="">Apakah anda yakin untuk memperbaharui status?</label>
        <input type="text" name="status" id="status"  class="form-control" readonly>
    </div>
    <input type="submit" class="btn btn-primary" value="Perbaharui"> <br>
        
    </form>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<!-- {{-- modal edit data end--}} -->
<?php
	 include_once('../layout/footer.php'); // Menyisipkan file footer dari folder layout
 }
 ?>
<script>
$(document).ready(function(){
  $("#formAdd").hide();
  $("#resultSearch").hide();
  });
function addData(){
    $('#myModal').modal('show');
}
function editData(table, id, base){
    var id_sim = $('#cariSimp input#id_sim').val();
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id_anggota: id_sim, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.error == '0') {
            $("#resultSearch").show("slow");
            $('table.spec_pinjaman tbody >tr').remove();
            no = 1;
            $.each(data.json, function(key, value){
                $('table.spec_pinjaman').append(
                '<tr>'+
                '<td>'+no+'</td>'+
                '<td>'+value[0]+'</td>'+
                '<td class="text-center">'+value[1]+'</td>'+
                '<td>Rp '+idr(value[2])+'</td>'+
                '<td class="text-center">'+value[3]+'</td>'+
                '<td class="text-center">'+value[4]+'</td>'+
                '</tr>'
                );
                no++;
            });
        } else{
          $("#resultSearch").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
// int to idr format
function idr(e) {
  var rp = parseInt(e);
    return new Intl.NumberFormat('ID').format(rp);
}
function search(base, table){
    var id_search = $('#cariData input#id_search').val();
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id_search, method: 'edit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $("#formAdd").show("slow");
            $("#formAdd #id_anggota").val(id_search);
            getNoPinjaman();

        } else{
            $("#formAdd").hide("slow");
            PNotify.error({
                title: 'Error',
                text: 'Error. ' + data.msg,
                type: 'error'
            });
        }
    }
    });
}
function getNoPinjaman() {
  // process the request
  $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
      data        :{table: 'pinjaman', no_pinjaman : 'no_pinjaman', method: 'generate'}, // our data object
      success: function (data) {
              data = JSON.parse(data);
              if(data.error == 0) {
                  $('#formAdd #no_pinjaman').val(data.json);
              }else{
                  PNotify.error({
                      title: 'Error',
                      text: 'Error. ' + data.msg,
                      type: 'error'
                  });
              }

          },error:function(){
              PNotify.error({
                  title: 'Error',
                  text: 'Error. Please check your connection',
                  type: 'error'
              });
          }
      });
  // stop the form from submitting the normal way and refreshing the page
}
// process the form
$('form#add_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#add_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
function getData(table, id, base){
    $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base +'config/process.php', // the url where we want to POST
    data: {table: table, id: id, method: 'getEdit'},
    dataType : 'json',
    success:function(data){
        if (data.msg == 'berhasil') {
            $('#editModal').modal('show');
            $('#edit_data input#id_data').val(data.json.no_pinjaman)
            if (data.json.status == 'lunas') {
              $('#edit_data input#status').val('belum lunas');;
            } else{
              $('#edit_data input#status').val('lunas');
            }
        }
    }
    });
}
// process edit form
$('form#edit_data').submit(function(event) {
// process the form
$.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo $base_url ?>config/process.php', // the url where we want to POST
        data        : $('form#edit_data').serialize(), // our data object
        success: function (data) {
                data = JSON.parse(data);
                if(data.error == 0) {
                    PNotify.success({
                      title: 'Success!',
                      text: data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                    });
                }

            },error:function(){
                PNotify.error({
                    title: 'Error',
                    text: 'Error. Please check your connection',
                    type: 'error'
                });
            }
        });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});
</script>