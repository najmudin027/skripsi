 <?php
session_start();
if(isset($_SESSION['hak_akses'])){
    if($_SESSION['hak_akses'] == 'admin'){
        header("Location: backend/index.php");
    } else{
        header("Location: frontend/index.php");
    }
} else{ 
include_once('layout/header.php');
?>
<!-- ===================================================== -->
<style type="text/css">
    .login-panel{
      margin-top: 5%;
    }
    body{
      background-color: #F8F8F8;
    }
    img{
      width: 200px;
      height: 200px;
    }
    .adm{
      color: #33A1D9;
    }
    </style>
<!-- ===================================================== -->
<body>
<div class="container">
<div class="row">
  <div class="login-panel col-md-4 col-md-offset-4 text-center">
    <img src="<?php echo $base_url ?>assets/images/koperasi.jpg" alt="Logo Administrator" class="img-circle">
    <h3>...:: KSP SDN KRUKUT 01 ::...</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post">
            <div class="form-group "><input class="form-control" placeholder="Masukan username atau id user anda" name="id_user" minlength="6" autocomplete="off"></div>
            <div class="form-group "><input class="form-control" placeholder="Masukan password" name="password" type="password" minlength="6" autocomplete="new-password"></div>
            <input type="hidden" name="login" value="login">
            <input class="btn btn-success btn-block" type="submit"  value="Login">
            <span class="adm">..:: Login security page ::..</span>
            </form>
        </div>
    </div>
</div>
</div>

<!-- Menyisipkan bagian bawah website dari external file -->
<?php
include_once('layout/footer.php');
?>
<script>
$(document).ready(function() {
// process the form
$('form').submit(function(event) {
    // process the form
    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'config/process.php', // the url where we want to POST
            data        : $('form').serialize(), // our data object
            success: function (data) {
                    data = JSON.parse(data);
                    if(data.error == 0) {
                        // if(data.json.hak_akses == 'admin'){
                        //     setTimeout(function(){ window.location.href= 'backend/index.php'; }, 1000);
                        // } else{
                            setTimeout(function(){ window.location.href= 'frontend/index.php'; }, 1000);
                        // }
                    }else{
                        PNotify.error({
                        title: 'Error',
                        text: 'Error. ' + data.msg,
                        type: 'error'
                        });
                    }

                },error:function(){
                    PNotify.error({
                        title: 'Error',
                        text: 'Error. Please check your connection',
                        type: 'error'
                    });
                }
            });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
  });
});
</script>
<?php } ?>