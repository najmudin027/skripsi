<?php
    $base_url = 'http://localhost/skripsi/www.ikgk.co.id/';
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Koperasi simpan pinjam ikgk sdn krukut 01</title>
    <!-- This is for icon website -->
    <link rel="shortcut icon" href="<?php echo $base_url ?>assets/images/koperasi.jpg">
    <!-- This is for package css external -->
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/fontawesome-free-5.5.0/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/datatables/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/node_modules/pnotify/dist/PNotifyBrightTheme.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/jquery-ui.css">
    <style>
    #footer {
        padding: 10px;
        font-size: 0.8em;
        text-align: center;
        border-top: 1px solid #C9E0ED;
        clear: both;
        height: 30px;
        text-align: center;
        padding-top: 7px;
        width: 100%;
        position: fixed;
        left: 0px;
        bottom: 0px;
        background-color: #F0F9FF;
    }
    </style>
</head>