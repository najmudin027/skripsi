<script src="<?php echo $base_url ?>assets/jquery-3.4.1.js"></script>
<div id="footer">
<?php 
if(isset($_SESSION['hak_akses']) && $_SESSION['hak_akses'] == 'kepala'){
?>
<script>
  $('small.btn').hide();
  $('a.btn.btn-danger').attr('disabled', true);
  $(':input[type="submit"]').hide();
  $('span.kepala').html('Kepala');
</script>
<?php 
} 
 ?>
Hak Cipta © <?php echo date('Y') ?>  <a href="#" tabindex="-1">Koperasi Simpan Pinjam IKGK SDN Krukut 01</a><br>
</div>
    <!-- This for javascript external -->
    <script src="<?php echo $base_url ?>assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $base_url ?>assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url ?>assets/fontawesome-free-5.5.0/js/all.min.js"></script>
    <script src="<?php echo $base_url ?>assets/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="<?php echo $base_url ?>assets/node_modules/pnotify/dist/umd/PNotify.js"></script>
    <script src="<?php echo $base_url ?>assets/node_modules/pnotify/dist/umd/PNotifyButtons.js"></script>
    <script src="<?php echo $base_url ?>assets/node_modules/pnotify/dist/umd/PNotifyConfirm.js"></script>
    <script src="<?php echo $base_url ?>assets/node_modules/pnotify/dist/umd/PNotifyHistory.js"></script>
    <script src="<?php echo $base_url ?>assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $base_url ?>assets/datatables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $base_url ?>assets/custom.js"></script>
</body>
</html>