PNotify.defaults.styling = 'bootstrap3'; // Bootstrap version 3
var user = $('.user').DataTable();
// int to idr format
function idr(e) {
  var rp = parseInt(e);
    return new Intl.NumberFormat('ID').format(rp);
}
$(document).ready(function(){
//  set datepicker
$("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
$("#datepicker1").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
//  set datepicker
$("#datepicker2").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
$("#datepicker3").datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
})
});
// hapus data
function destroyData(table, id, base){
var notice = PNotify.notice({
  title: 'Dibutuhkan konfirmasi.',
  text: 'Apakah anda yakin menghapus data yang anda pilih?',
  icon: 'fas fa-question-circle',
  hide: false,
  stack: {
    'dir1': 'down',
    'modal': true,
    'firstpos1': 25
  },
  modules: {
    Confirm: {
      confirm: true
    },
    Buttons: {
      closer: false,
      sticker: false
    },
    History: {
      history: false
    },
  }
});
notice.on('pnotify.confirm', function() {
  $.ajax({
    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
    url         : base , // the url where we want to POST
    data: {table: table, field: id, method: 'delete'},
    dataType : 'json',
    success:function(data){
        if (data.error == '0') {
            PNotify.success({
			  title: 'Success!',
			  text: data.json
			});
            setTimeout(function(){ location.reload(); }, 1000);
        }
      }	
    });	
});
notice.on('pnotify.cancel', function() {
});
}